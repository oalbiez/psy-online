package eu.keilana.backend.library.test.property;

import io.vavr.test.Gen;

import java.time.Duration;
import java.time.LocalDate;

public final class Generators {
    public static Gen<LocalDate> localDate() {
        return random -> LocalDate.ofEpochDay(random.nextInt(50000));
    }

    public static Gen<Duration> duration() {
        return duration(60 * 72);
    }

    public static Gen<Duration> duration(final int minutes) {
        return random -> Duration.ofMinutes(random.nextInt(minutes));
    }

    private Generators() {
    }
}

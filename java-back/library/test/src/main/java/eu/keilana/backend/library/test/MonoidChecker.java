package eu.keilana.backend.library.test;

import io.vavr.test.Arbitrary;
import io.vavr.test.Gen;
import io.vavr.test.Property;
import org.checkerframework.checker.nullness.qual.NonNull;

public final class MonoidChecker {
    public interface Definition<T> {

        Gen<T> element();

        T zero();

        T compose(T left, T right);

    }

    @SuppressWarnings("nullness")
    public static <T> void checkMonoid(final @NonNull Definition<T> monoid) {
        final var element = Arbitrary.ofAll(monoid.element());

        Property.def("a monoid should have a neutral element")
                .forAll(element)
                .suchThat(c -> c.equals(monoid.compose(c, monoid.zero())) && c.equals(monoid.compose(monoid.zero(), c)))
                .check()
                .assertIsSatisfied();

        Property.def("a monoid should be associative")
                .forAll(element, element, element)
                .suchThat((a, b, c) -> monoid.compose(a, monoid.compose(b, c)).equals(monoid.compose(monoid.compose(a, b), c)))
                .check()
                .assertIsSatisfied();
    }

    private MonoidChecker() {
    }
}

package eu.keilana.backend.infra.spi;

import com.google.common.io.Files;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.adapter.SessionDSL;
import eu.keilana.backend.session.domain.model.Session;
import eu.keilana.backend.session.domain.usecase.SessionRepository;
import io.vavr.collection.List;
import io.vavr.control.Option;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;

import static eu.keilana.backend.session.adapter.SessionDSL.renderer;
import static java.nio.charset.StandardCharsets.UTF_8;

public final class FileSessionRepository implements SessionRepository {
    private final Path path;

    public static FileSessionRepository create(final Path path) {
        return new FileSessionRepository(path);
    }

    private FileSessionRepository(final Path path) {
        this.path = path;
    }

    public List<Session> all() {
        return allSessions().map(this::find).flatMap(Option::iterator).sortBy(Session::when);
    }

    public List<Session> allBetween(final LocalDate from, final LocalDate to) {
        return all().filter(s -> s.when().isAfter(from) && s.when().isBefore(to));
    }

    public Option<Session> find(final Identifier identifier) {
        return read(resolve(identifier)).map(s -> s.clone(identifier));
    }

    public Session save(final Session session) {
        try {
            Files.asCharSink(resolve(session.identifier()), UTF_8).write(renderer().render(session));
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return session;
    }

    public boolean delete(final Identifier identifier) {
        return resolve(identifier).delete();
    }

    private List<Identifier> allSessions() {
        final File[] files = path.toFile().listFiles((dir, name) -> name.endsWith(".session"));
        if (files == null) {
            return List.of();
        }
        //noinspection UnstableApiUsage
        return List.of(files)
                .map(File::getName)
                .map(Files::getNameWithoutExtension)
                .map(Identifier::fromString);
    }

    private File resolve(final Identifier identifier) {
        return path.resolve(identifier.asString() + ".session").toFile();
    }

    private Option<Session> read(final File file) {
        try {
            return SessionDSL.parse(Files.asCharSource(file, UTF_8).read()).toOption();
        } catch (final IOException e) {
            return Option.none();
        }
    }
}

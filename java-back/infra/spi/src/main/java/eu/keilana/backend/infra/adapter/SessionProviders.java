package eu.keilana.backend.infra.adapter;

import eu.keilana.backend.report.domain.model.Category;
import eu.keilana.backend.report.domain.model.Session;
import eu.keilana.backend.report.domain.usecase.SessionProvider;
import eu.keilana.backend.session.domain.usecase.SessionRepository;
import io.vavr.collection.List;

import java.time.LocalDate;

public final class SessionProviders {

    public static SessionProvider fromSessionRepository(final SessionRepository repository) {
        return new SessionProvider() {
            public List<Session> allBetween(final LocalDate from, final LocalDate to) {
                return repository.allBetween(from, to).map(this::adapt);
            }

            private Session adapt(final eu.keilana.backend.session.domain.model.Session session) {
                return Session.session(
                        session.when(),
                        session.duration(),
                        Category.valueOf(session.category().name()));
            }
        };
    }

    private SessionProviders() {
    }
}

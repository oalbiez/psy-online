package eu.keilana.backend.infra.spi;

import com.google.common.io.Files;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.payment.domain.adapter.PaymentDSL;
import eu.keilana.backend.payment.domain.model.Payment;
import eu.keilana.backend.payment.domain.usecase.PaymentRepository;
import io.vavr.collection.List;
import io.vavr.control.Option;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;

import static eu.keilana.backend.payment.domain.adapter.PaymentDSL.renderer;
import static java.nio.charset.StandardCharsets.UTF_8;

public final class FilePaymentRepository implements PaymentRepository {
    private final Path path;

    public static FilePaymentRepository create(final Path path) {
        return new FilePaymentRepository(path);
    }

    private FilePaymentRepository(final Path path) {
        this.path = path;
    }

    public List<Payment> all() {
        return allPayments().map(this::find).flatMap(Option::iterator).sortBy(Payment::when);
    }

    public List<Payment> allBetween(final LocalDate from, final LocalDate to) {
        return all().filter(s -> s.when().isAfter(from) && s.when().isBefore(to));
    }

    public Option<Payment> find(final Identifier identifier) {
        return read(resolve(identifier)).map(s -> s.clone(identifier));
    }

    public Payment save(final Payment payment) {
        try {
            Files.asCharSink(resolve(payment.identifier()), UTF_8).write(renderer().render(payment));
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return payment;
    }

    public boolean delete(final Identifier identifier) {
        return resolve(identifier).delete();
    }

    private List<Identifier> allPayments() {
        final File[] files = path.toFile().listFiles((dir, name) -> name.endsWith(".payment"));
        if (files == null) {
            return List.of();
        }
        //noinspection UnstableApiUsage
        return List.of(files)
                .map(File::getName)
                .map(Files::getNameWithoutExtension)
                .map(Identifier::fromString);
    }

    private File resolve(final Identifier identifier) {
        return path.resolve(identifier.asString() + ".payment").toFile();
    }

    private Option<Payment> read(final File file) {
        try {
            return PaymentDSL.parse(Files.asCharSource(file, UTF_8).read()).toOption();
        } catch (final IOException e) {
            return Option.none();
        }
    }
}

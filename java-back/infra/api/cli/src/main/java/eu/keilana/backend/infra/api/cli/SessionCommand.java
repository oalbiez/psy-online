package eu.keilana.backend.infra.api.cli;

import com.google.common.io.Files;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.infra.api.cli.Parameters.CategoryParam;
import eu.keilana.backend.infra.api.cli.Parameters.DateParam;
import eu.keilana.backend.infra.api.cli.Parameters.DurationParam;
import eu.keilana.backend.infra.api.cli.Parameters.IdentifierParam;
import eu.keilana.backend.session.adapter.SessionDSL;
import eu.keilana.backend.session.domain.model.Session;
import eu.keilana.backend.session.domain.usecase.*;
import io.vavr.collection.List;
import io.vavr.control.Option;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;

import java.io.File;
import java.io.IOException;

import static eu.keilana.backend.session.adapter.SessionDSL.renderer;
import static java.nio.charset.StandardCharsets.UTF_8;

@Command(name = "session", description = "session commands")
public final class SessionCommand {

    @ParentCommand
    private Main parent;

    SessionCommand() {
    }

    @Command(name = "create", description = "create a new session")
    public int createSession(@Mixin final DateParam date,
                             @Mixin final DurationParam duration,
                             @Mixin final CategoryParam category) {
        session(CreateSession
                .build(sessionRepository(), Identifier.generateUUID())
                .create(date.date, duration.duration, category.category));
        return 0;
    }

    @Command(name = "categorize", description = "categorize a session")
    public int categorizeSession(
            @Mixin final IdentifierParam identifier,
            @Mixin final CategoryParam category) {
        final var r = CategorizeSession
                .build(sessionRepository())
                .categorize(identifier.identifier, category.category);
        session(r);
        if (r.isEmpty()) {
            return 1;
        }
        return 0;
    }

    @Command(name = "delete", description = "delete a session")
    public int deleteSession(@Mixin final IdentifierParam identifier) {
        if (DeleteSession.build(sessionRepository()).delete(identifier.identifier)) {
            System.out.println("Session deleted");
            return 0;
        } else {
            System.out.println("Session not found");
            return 1;
        }
    }

    @Command(name = "export", description = "export sessions")
    public int exportSessions(@Parameters(paramLabel = "<file>", description = "destination") final File target) throws IOException {
        Files.asCharSink(target, UTF_8).writeLines(AllSessions.build(sessionRepository()).allSessions().map(renderer().withIdentifier()::render));
        return 0;
    }

    @Command(name = "import", description = "import sessions")
    public int importSessions(@Parameters(paramLabel = "<file>", description = "source") final File target) throws IOException {
        List.ofAll(Files.asCharSource(target, UTF_8).readLines()).forEach(s -> {
            final var result = SessionDSL.parse(s).map(sessionRepository()::save);
            if (result.isRight()) {
                System.out.println("Importing " + s + ": OK");
            } else {
                System.out.println("Importing " + s + ": " + result.getLeft());
            }
        });
        return 0;
    }

    @Command(name = "list", description = "list all sessions")
    public int listSessions() {
        sessions(AllSessions.build(sessionRepository()).allSessions());
        return 0;
    }

    @Command(name = "move", description = "move a session")
    public int moveSession(@Mixin final IdentifierParam identifier,
                           @Mixin final DateParam date) {
        final var r = MoveSession
                .build(sessionRepository())
                .move(identifier.identifier, date.date);
        session(r);
        if (r.isEmpty()) {
            return 1;
        }
        return 0;
    }

    SessionRepository sessionRepository() {
        return parent.sessionRepository();
    }

    public static void sessions(final List<Session> sessions) {
        sessions.forEach(SessionCommand::session);
    }

    public static void session(final Option<Session> session) {
        System.out.println(session.map(renderer().withIdentifier()::render).getOrElse("No session"));
    }

    public static void session(final Session session) {
        System.out.println(renderer().withIdentifier().render(session));
    }
}

package eu.keilana.backend.infra.api.cli;


import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import eu.keilana.backend.infra.spi.FilePaymentRepository;
import eu.keilana.backend.infra.spi.FileSessionRepository;
import eu.keilana.backend.payment.domain.usecase.PaymentRepository;
import eu.keilana.backend.session.domain.usecase.SessionRepository;
import picocli.CommandLine;
import picocli.CommandLine.HelpCommand;

import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDate;

@CommandLine.Command(
        description = "keilana management",
        name = "keilana",
        mixinStandardHelpOptions = true,
        version = "keilana 1.0",
        subcommands = {
                HelpCommand.class,
                SessionCommand.class,
                PaymentCommand.class,
                ReportCommand.class})
public final class Main {

    @CommandLine.Option(names = {"-db"}, description = "repositories path", defaultValue = "./db")
    Path repositories;

    SessionRepository sessionRepository() {
        return FileSessionRepository.create(repositories.resolve("sessions"));
    }

    PaymentRepository paymentRepository() {
        return FilePaymentRepository.create(repositories.resolve("payments"));
    }

    public static void main(final String[] args) {
        new CommandLine(new Main())
                .setCaseInsensitiveEnumValuesAllowed(false)

                .registerConverter(Duration.class, Converters.duration())
                .registerConverter(Identifier.class, Converters.identifier())
                .registerConverter(LocalDate.class, Converters.date())
                .registerConverter(Price.class, Converters.price())
                .execute(args);
    }

    private Main() {
    }
}

package eu.keilana.backend.infra.api.cli;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import picocli.CommandLine;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public final class Converters {

    static CommandLine.ITypeConverter<LocalDate> date() {
        return value -> {
            if ("now".equals(value)) {
                return LocalDate.now();
            }
            try {
                return LocalDate.parse(value);
            } catch (final DateTimeParseException ignored) {
            }
            throw new CommandLine.TypeConversionException("");
        };
    }

    static CommandLine.ITypeConverter<Duration> duration() {
        return value -> {
            try {
                return Duration.parse(value);
            } catch (final DateTimeParseException ignored) {
            }
            try {
                return Duration.ofMinutes(Long.parseLong(value));
            } catch (final NumberFormatException ignored) {
            }
            throw new CommandLine.TypeConversionException("");
        };
    }

    static CommandLine.ITypeConverter<Identifier> identifier() {
        return Identifier::fromString;
    }

    static CommandLine.ITypeConverter<Price> price() {
        return value -> {
            try {
                return Price.parse(value);
            } catch (final NumberFormatException ignored) {
            }
            throw new CommandLine.TypeConversionException("");
        };
    }

    private Converters() {
    }
}

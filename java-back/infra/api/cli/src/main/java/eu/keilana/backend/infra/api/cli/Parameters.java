package eu.keilana.backend.infra.api.cli;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import eu.keilana.backend.session.domain.model.Category;
import picocli.CommandLine;

import java.time.Duration;
import java.time.LocalDate;

@SuppressWarnings("PublicConstructor")
public final class Parameters {

    public static final class CategoryParam {
        @CommandLine.Parameters(description = "category (Valid values: ${COMPLETION-CANDIDATES})", type = Category.class, defaultValue = "INDIVIDUAL")
        Category category;
    }

    public static final class DateParam {
        @CommandLine.Parameters(description = "date", defaultValue = "now")
        LocalDate date;
    }

    public static final class DurationParam {
        @CommandLine.Parameters(description = "duration", defaultValue = "60")
        Duration duration;
    }

    public static final class IdentifierParam {
        @CommandLine.Parameters(description = "identifier")
        Identifier identifier;
    }

    public static final class PriceParam {
        @CommandLine.Parameters(description = "price")
        Price price;
    }

    private Parameters() {
    }
}

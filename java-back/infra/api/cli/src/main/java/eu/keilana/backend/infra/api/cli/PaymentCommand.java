package eu.keilana.backend.infra.api.cli;

import com.google.common.io.Files;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.infra.api.cli.Parameters.DateParam;
import eu.keilana.backend.infra.api.cli.Parameters.IdentifierParam;
import eu.keilana.backend.infra.api.cli.Parameters.PriceParam;
import eu.keilana.backend.payment.domain.adapter.PaymentDSL;
import eu.keilana.backend.payment.domain.model.Payment;
import eu.keilana.backend.payment.domain.usecase.*;
import io.vavr.collection.List;
import io.vavr.control.Option;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.ParentCommand;

import java.io.File;
import java.io.IOException;

import static eu.keilana.backend.payment.domain.adapter.PaymentDSL.renderer;
import static java.nio.charset.StandardCharsets.UTF_8;

@Command(
        name = "payment",
        description = "payment commands",
        subcommands = {PaymentCommand.Correct.class})
public final class PaymentCommand {
    @ParentCommand
    private Main parent;

    PaymentCommand() {
    }

    @Command(name = "create", description = "create a new payment")
    public int createSession(@Mixin final DateParam date,
                             @Mixin final PriceParam price) {
        payment(CreatePayment
                .build(paymentRepository(), Identifier.generateUUID())
                .create(date.date, price.price));
        return 0;
    }

    @Command(name = "list", description = "list all payments")
    public int listPayments() {
        payments(AllPayments.build(paymentRepository()).allPayments());
        return 0;
    }

    @Command(name = "correct", description = "correct payment")
    static class Correct {
        @ParentCommand
        private PaymentCommand parent;

        @Command(name = "amount", description = "correct payment amount")
        int amount(
                @Mixin final IdentifierParam identifier,
                @Mixin final PriceParam price) {
            final var r = CorrectAmountPayment
                    .build(paymentRepository())
                    .correct(identifier.identifier, price.price);
            payment(r);
            if (r.isEmpty()) {
                return 1;
            }
            return 0;
        }

        @Command(name = "date", description = "correct payment date")
        int date(
                @Mixin final IdentifierParam identifier,
                @Mixin final DateParam date) {
            final var r = CorrectDatePayment
                    .build(paymentRepository())
                    .correct(identifier.identifier, date.date);
            payment(r);
            if (r.isEmpty()) {
                return 1;
            }
            return 0;
        }

        PaymentRepository paymentRepository() {
            return parent.paymentRepository();
        }
    }


    @Command(name = "export", description = "export payments")
    public int exportPayments(@CommandLine.Parameters(paramLabel = "<file>", description = "destination") final File target) throws IOException {
        Files.asCharSink(target, UTF_8).writeLines(AllPayments.build(paymentRepository()).allPayments().map(PaymentDSL.renderer().withIdentifier()::render));
        return 0;
    }

    @Command(name = "import", description = "import payments")
    public int importPayments(@CommandLine.Parameters(paramLabel = "<file>", description = "source") final File target) throws IOException {
        List.ofAll(Files.asCharSource(target, UTF_8).readLines()).forEach(s -> {
            final var result = PaymentDSL.parse(s).map(paymentRepository()::save);
            if (result.isRight()) {
                System.out.println("Importing " + s + ": OK");
            } else {
                System.out.println("Importing " + s + ": " + result.getLeft());
            }
        });
        return 0;
    }


    PaymentRepository paymentRepository() {
        return parent.paymentRepository();
    }

    public static void payments(final List<Payment> payments) {
        payments.forEach(PaymentCommand::payment);
    }

    public static void payment(final Option<Payment> payment) {
        System.out.println(payment.map(renderer().withIdentifier()::render).getOrElse("No payment"));
    }

    public static void payment(final Payment payment) {
        System.out.println(renderer().withIdentifier().render(payment));
    }
}

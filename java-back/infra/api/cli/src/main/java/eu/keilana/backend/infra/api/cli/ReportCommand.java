package eu.keilana.backend.infra.api.cli;

import eu.keilana.backend.report.domain.model.Category;
import eu.keilana.backend.report.domain.usecase.SessionProvider;
import eu.keilana.backend.report.domain.usecase.SessionStatistics;
import eu.keilana.backend.session.domain.usecase.SessionRepository;
import io.vavr.collection.List;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.ParentCommand;

import java.time.Duration;
import java.time.LocalDate;

import static eu.keilana.backend.infra.adapter.SessionProviders.fromSessionRepository;

@Command(name = "report", description = "reports commands")
public final class ReportCommand {

    @ParentCommand
    private Main parent;

    ReportCommand() {
    }

    @Command(name = "sessions", description = "Sessions statistics")
    public int sessions(
            @CommandLine.Parameters(paramLabel = "<from>", description = "from date") final LocalDate from,
            @CommandLine.Parameters(paramLabel = "<to>", description = "from date") final LocalDate to) {
        final var statistics = SessionStatistics.build(sessionProvider()).report(from, to);
        List.of(Category.values()).forEach(c -> System.out.println(c.name() + ": " + render(statistics.of(c))));
        System.out.println("Total: " + render(statistics.of(Category.values())));
        return 0;
    }

    private static String render(final Duration duration) {
        return duration.toHours() + ":" + duration.toMinutesPart();
    }

    SessionProvider sessionProvider() {
        return fromSessionRepository(sessionRepository());
    }

    SessionRepository sessionRepository() {
        return parent.sessionRepository();
    }
}

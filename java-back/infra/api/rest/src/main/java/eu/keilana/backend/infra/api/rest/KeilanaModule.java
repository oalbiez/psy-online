package eu.keilana.backend.infra.api.rest;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import eu.keilana.backend.payment.domain.model.Payment;
import eu.keilana.backend.session.domain.model.Session;

import java.io.IOException;

public final class KeilanaModule {
    public static final Version VERSION = new Version(1, 0, 0, null, null, null);

    public static SimpleModule keilanaModule() {
        return new SimpleModule("KeilanaSerializer", VERSION)
                .addSerializer(Session.class, new StdSerializer<>(Session.class) {
                    @Override
                    public void serialize(final Session value, final JsonGenerator gen, final SerializerProvider provider) throws IOException {
                        gen.writeStartObject();
                        gen.writeStringField("id", value.identifier().asString());
                        gen.writeStringField("when", value.when().toString());
                        gen.writeNumberField("duration", value.duration().toMinutes());
                        gen.writeStringField("category", value.category().toString());
                        gen.writeEndObject();
                    }
                })
                .addSerializer(Payment.class, new StdSerializer<>(Payment.class) {
                    @Override
                    public void serialize(final Payment value, final JsonGenerator gen, final SerializerProvider provider) throws IOException {
                        gen.writeStartObject();
                        gen.writeStringField("id", value.identifier().asString());
                        gen.writeStringField("when", value.when().toString());
                        gen.writeNumberField("amount", value.amount().normalizedValue());
                        gen.writeEndObject();
                    }
                });
    }

    private KeilanaModule() {
    }
}

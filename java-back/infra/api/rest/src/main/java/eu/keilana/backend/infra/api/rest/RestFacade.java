package eu.keilana.backend.infra.api.rest;

import eu.keilana.backend.payment.domain.usecase.AllPayments;
import eu.keilana.backend.payment.domain.usecase.PaymentRepository;
import eu.keilana.backend.session.domain.usecase.AllSessions;
import eu.keilana.backend.session.domain.usecase.SessionRepository;
import io.javalin.http.Handler;

public final class RestFacade {
    private final SessionRepository sessions;
    private final PaymentRepository payments;

    public static RestFacade facade(final SessionRepository sessions, final PaymentRepository payments) {
        return new RestFacade(sessions, payments);
    }

    private RestFacade(final SessionRepository sessions, final PaymentRepository payments) {
        this.sessions = sessions;
        this.payments = payments;
    }

    public Handler allSessions() {
        return ctx -> ctx.json(AllSessions.build(sessions).allSessions().toJavaList());
    }

    public Handler allPayments() {
        return ctx -> ctx.json(AllPayments.build(payments).allPayments().toJavaList());
    }
}

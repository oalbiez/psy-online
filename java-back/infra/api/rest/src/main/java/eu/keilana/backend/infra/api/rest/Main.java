package eu.keilana.backend.infra.api.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.keilana.backend.infra.spi.FilePaymentRepository;
import eu.keilana.backend.infra.spi.FileSessionRepository;
import eu.keilana.backend.payment.domain.usecase.PaymentRepository;
import eu.keilana.backend.session.domain.usecase.SessionRepository;
import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;

import java.nio.file.Path;

import static eu.keilana.backend.infra.api.rest.KeilanaModule.keilanaModule;

public final class Main {
    public static void main(final String[] args) {
        JavalinJackson.configure(new ObjectMapper().registerModule(keilanaModule()));
        final Javalin app = Javalin.create().start(7000);
        final RestFacade facade = RestFacade.facade(sessionRepository(), paymentRepository());
        app.get("/sessions", facade.allSessions());
        app.get("/payments", facade.allPayments());
    }

    static SessionRepository sessionRepository() {
        return FileSessionRepository.create(Path.of("db", "sessions"));
    }

    static PaymentRepository paymentRepository() {
        return FilePaymentRepository.create(Path.of("db", "payments"));
    }

    private Main() {
    }
}

package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Category;
import eu.keilana.backend.session.domain.model.Session;
import io.vavr.control.Option;

import static eu.keilana.backend.session.domain.usecase.SessionUpdater.update;

public final class CategorizeSession {
    private final SessionRepository sessions;

    public static CategorizeSession build(final SessionRepository sessions) {
        return new CategorizeSession(sessions);
    }

    private CategorizeSession(final SessionRepository sessions) {
        this.sessions = sessions;
    }

    public Option<Session> categorize(final Identifier identifier, final Category category) {
        return update(sessions, identifier, s -> s.category(category));
    }
}

package eu.keilana.backend.session.domain.usecase;

import com.google.common.collect.Maps;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Session;
import io.vavr.collection.List;
import io.vavr.control.Option;

import java.time.LocalDate;
import java.util.Map;

public final class Default {


    public static SessionRepository inMemorySessions() {
        return new SessionRepository() {
            private final Map<Identifier, Session> sessions = Maps.newHashMap();

            public List<Session> all() {
                return List.ofAll(sessions.values()).sortBy(Session::when);
            }

            public List<Session> allBetween(final LocalDate from, final LocalDate to) {
                return all().filter(s -> s.when().isAfter(from) && s.when().isBefore(to));
            }

            public Option<Session> find(final Identifier identifier) {
                return Option.of(sessions.get(identifier));
            }

            public Session save(final Session session) {
                sessions.put(session.identifier(), session);
                return session;
            }

            public boolean delete(final Identifier identifier) {
                sessions.remove(identifier);
                return true;
            }
        };
    }

    private Default() {
    }
}

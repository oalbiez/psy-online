package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.session.domain.model.Session;
import io.vavr.collection.List;

import java.time.LocalDate;

public final class AllSessions {
    private final SessionRepository sessions;

    public static AllSessions build(final SessionRepository sessions) {
        return new AllSessions(sessions);
    }

    private AllSessions(final SessionRepository sessions) {
        this.sessions = sessions;
    }

    public List<Session> allSessions() {
        return sessions.all();
    }

    public List<Session> allSessionsBetween(final LocalDate from, final LocalDate to) {
        return sessions.allBetween(from, to);
    }
}

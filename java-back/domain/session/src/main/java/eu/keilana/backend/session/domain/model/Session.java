package eu.keilana.backend.session.domain.model;

import eu.keilana.backend.domain.type.Identifier;

import java.time.Duration;
import java.time.LocalDate;

import static java.time.Duration.ofMinutes;

public final class Session {
    private final Identifier identifier;
    private final LocalDate when;
    private final Duration duration;
    private final Category category;

    public static Session session() {
        return session(Identifier.uuid());
    }

    public static Session session(final Identifier identifier) {
        return new Session(identifier, LocalDate.EPOCH, ofMinutes(60), Category.INDIVIDUAL);
    }

    private Session(final Identifier identifier, final LocalDate when, final Duration duration, final Category category) {
        this.identifier = identifier;
        this.when = when;
        this.duration = duration;
        this.category = category;
    }

    public Session clone(final Identifier newIdentifier) {
        return new Session(newIdentifier, when, duration, category);
    }

    public Session during(final Duration duration) {
        return new Session(identifier, when, duration, category);
    }

    public Session move(final LocalDate when) {
        return new Session(identifier, when, duration, category);
    }

    public Session on(final LocalDate when) {
        return new Session(identifier, when, duration, category);
    }

    public Session category(final Category category) {
        return new Session(identifier, when, duration, category);
    }

    public Identifier identifier() {
        return identifier;
    }

    public LocalDate when() {
        return when;
    }

    public Duration duration() {
        return duration;
    }

    public Category category() {
        return category;
    }

    @Override
    public String toString() {
        return "Session{" +
                "identifier=" + identifier +
                ", when=" + when +
                ", duration=" + duration +
                ", category=" + category +
                '}';
    }
}

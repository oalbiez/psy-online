package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.CRUD;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Session;
import io.vavr.collection.List;

import java.time.LocalDate;

public interface SessionRepository extends CRUD<Identifier, Session> {
    List<Session> allBetween(final LocalDate from, final LocalDate to);
}

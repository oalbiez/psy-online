package eu.keilana.backend.session.domain.usecase;


import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Category;
import eu.keilana.backend.session.domain.model.Session;

import java.time.Duration;
import java.time.LocalDate;

import static eu.keilana.backend.session.domain.model.Session.session;


public final class CreateSession {
    private final SessionRepository sessions;
    private final Identifier.Generator identifiers;

    public static CreateSession build(final SessionRepository sessions, final Identifier.Generator identifiers) {
        return new CreateSession(sessions, identifiers);
    }

    private CreateSession(final SessionRepository sessions, final Identifier.Generator identifiers) {
        this.sessions = sessions;
        this.identifiers = identifiers;
    }

    public Session create(final LocalDate when, final Duration duration, final Category category) {
        return sessions.save(session(identifiers.get()).on(when).during(duration).category(category));
    }
}

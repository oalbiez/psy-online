package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Session;
import io.vavr.control.Option;

import java.time.LocalDate;

import static eu.keilana.backend.session.domain.usecase.SessionUpdater.update;

public final class MoveSession {
    private final SessionRepository sessions;

    public static MoveSession build(final SessionRepository sessions) {
        return new MoveSession(sessions);
    }

    private MoveSession(final SessionRepository sessions) {
        this.sessions = sessions;
    }

    public Option<Session> move(final Identifier identifier, final LocalDate when) {
        return update(sessions, identifier, s -> s.move(when));
    }
}

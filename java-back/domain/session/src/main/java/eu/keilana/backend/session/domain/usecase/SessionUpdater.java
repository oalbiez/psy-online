package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Session;
import io.vavr.control.Option;

import java.util.function.Function;

final class SessionUpdater {
    public static Option<Session> update(final SessionRepository sessions, final Identifier identifier, final Function<Session, Session> action) {
        return sessions.find(identifier)
                .map(action)
                .map(sessions::save);
    }

    private SessionUpdater() {
    }
}

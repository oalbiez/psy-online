package eu.keilana.backend.session.domain.model;

import org.checkerframework.checker.nullness.qual.Nullable;

public enum Category {
    WORKSHOP, INDIVIDUAL, VOLUNTEER;

    public static Category parse(final @Nullable String value) {
        if (value == null) {
            throw new IllegalArgumentException("category cannot be null");
        }
        return Category.valueOf(value.toUpperCase());
    }
}

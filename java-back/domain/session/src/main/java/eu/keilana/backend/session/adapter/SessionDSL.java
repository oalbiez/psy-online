package eu.keilana.backend.session.adapter;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Category;
import eu.keilana.backend.session.domain.model.Session;
import io.vavr.control.Either;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.petitparser.context.Token;

import java.time.Duration;
import java.time.LocalDate;

import static eu.keilana.backend.session.domain.model.Session.session;
import static org.petitparser.parser.primitive.CharacterParser.*;
import static org.petitparser.parser.primitive.StringParser.ofIgnoringCase;

public final class SessionDSL {

    public static Either<String, Session> parse(final @NonNull String declaration) {
        final var id = of('[').seq(word().or(of('-')).plus().flatten().token(), of(']')).pick(1).trim();
        final var category = ofIgnoringCase("individual")
                .or(ofIgnoringCase("workshop"), ofIgnoringCase("volunteer"))
                .trim().token();
        final var date = digit().repeat(4, 5).seq(of('-'), digit().times(2), of('-'), digit().times(2)).flatten().trim().token();
        final var duration = digit().plus().flatten().trim().token().seq(of('M').trim()).pick(0);

        final var builder = new SessionBuilder();
        final var session = id.mapWithSideEffects(builder::identifier).optional()
                .seq(
                        category.mapWithSideEffects(builder::category),
                        ofIgnoringCase("on"),
                        date.mapWithSideEffects(builder::on),
                        ofIgnoringCase("of"),
                        duration.mapWithSideEffects(builder::during),
                        of('#').seq(any().star()).optional())
                .trim().end();

        final var parse = session.parse(declaration);
        if (parse.isSuccess()) {
            return Either.right(builder.session);
        }
        return Either.left(parse.toString());
    }

    public interface Renderer {
        Renderer withIdentifier();

        String render(final Session session);
    }

    public static Renderer renderer() {
        return new Renderer() {
            private boolean withIdentifier = false;

            public Renderer withIdentifier() {
                withIdentifier = true;
                return this;
            }

            public String render(final Session session) {
                if (withIdentifier) {
                    return "[" + session.identifier().asString() + "] " + internal(session);
                }
                return internal(session);
            }

            private String internal(final Session session) {
                return session.category().name().toLowerCase() +
                        " on " +
                        session.when().toString() +
                        " of " +
                        session.duration().toMinutes() +
                        "M";
            }
        };
    }


    private static class SessionBuilder {
        Session session = session();

        Token identifier(final Token token) {
            session = session.clone(Identifier.fromString(token.getValue()));
            return token;
        }

        Token on(final Token token) {
            session = session.on(LocalDate.parse(token.getValue()));
            return token;
        }

        Token during(final Token token) {
            session = session.during(Duration.ofMinutes(Long.parseLong(token.getValue())));
            return token;
        }

        Token category(final Token token) {
            session = session.category(Category.parse(token.getValue()));
            return token;
        }
    }

    private SessionDSL() {
    }
}

package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;

public final class DeleteSession {
    private final SessionRepository sessions;

    public static DeleteSession build(final SessionRepository sessions) {
        return new DeleteSession(sessions);
    }

    private DeleteSession(final SessionRepository sessions) {
        this.sessions = sessions;
    }

    public boolean delete(final Identifier identifier) {
        return sessions.delete(identifier);
    }
}

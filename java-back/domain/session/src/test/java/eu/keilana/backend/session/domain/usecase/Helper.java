package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.session.adapter.SessionDSL;
import io.vavr.collection.List;
import io.vavr.control.Either;

import static eu.keilana.backend.domain.type.CrudLoader.load;

final class Helper {
    static SessionRepository repository(final String... sessions) {
        final var repository = Default.inMemorySessions();
        load(repository, List.of(sessions).map(SessionDSL::parse).flatMap(Either::iterator));
        return repository;
    }

    static List<String> contentOf(final SessionRepository repository) {
        return AllSessions.build(repository).allSessions().map(SessionDSL.renderer().withIdentifier()::render);
    }

    private Helper() {
    }
}

package eu.keilana.backend.session.adapter;

import eu.keilana.backend.session.domain.model.Category;
import eu.keilana.backend.session.domain.model.Session;
import io.vavr.control.Either;
import io.vavr.test.Arbitrary;
import io.vavr.test.Gen;
import io.vavr.test.Property;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;

import static eu.keilana.backend.library.test.property.Generators.localDate;
import static eu.keilana.backend.session.adapter.SessionDSL.parse;
import static eu.keilana.backend.session.adapter.SessionDSL.renderer;
import static eu.keilana.backend.session.domain.model.Session.session;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SessionDSLShould {

    @Test
    public void be_able_to_parse_and_render_with_identifier() {
        final var renderer = renderer().withIdentifier();
        Property.def("parse and render with identifier")
                .forAll(Arbitrary.ofAll(anySession()).map(renderer::render))
                .suchThat(s -> parse(s).map(renderer::render).equals(Either.right(s)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    public void be_able_to_parse_and_render_without_identifier() {
        Property.def("parse and render without identifier")
                .forAll(Arbitrary.ofAll(anySession()).map(renderer()::render))
                .suchThat(s -> parse(s).map(renderer()::render).equals(Either.right(s)))
                .check()
                .assertIsSatisfied();
    }

    @ParameterizedTest()
    @ValueSource(strings = {
            "#",
            " # ",
            " # toto"
    })
    public void be_able_to_parse_with_comments(final String comment) {
        final var session = "workshop on 2020-02-12 of 180M";
        assertEquals(Either.right(session), parse(session + comment).map(SessionDSL.renderer()::render));
    }

    @Test
    public void be_able_reject_invalid_parsing() {
        assertEquals(Either.left("Failure[1:1]: volunteer expected"), parse("invalid"));
    }

    private static Gen<Session> anySession() {
        return random -> session()
                .on(localDate().apply(random))
                .during(Duration.ofMinutes(random.nextInt(60 * 48)))
                .category(Category.values()[random.nextInt(Category.values().length)]);
    }
}
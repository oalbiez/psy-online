package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.session.domain.model.Session;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static eu.keilana.backend.session.adapter.SessionDSL.renderer;
import static eu.keilana.backend.session.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("SameParameterValue")
class AllSessionsShould {
    private SessionRepository repository = Default.inMemorySessions();
    private List<Session> sessions = List.empty();

    @Test
    public void list_all_session_in_repository() {
        given_sessions(
                "{when:2020-02-12, category:workshop, duration:180M}",
                "workshop on 2020-02-12 of 180M",
                "volunteer on 2020-03-04 of 180M",
                "individual on 2020-04-14 of 60M");
        when_I_list_all_sessions();
        then_listed_sessions_should_be(
                "workshop on 2020-02-12 of 180M",
                "volunteer on 2020-03-04 of 180M",
                "individual on 2020-04-14 of 60M");
    }

    @Test
    public void list_all_session_in_repository_between_two_dates() {
        given_sessions(
                "workshop on 2020-02-12 of 180M",
                "volunteer on 2020-03-04 of 180M",
                "individual on 2020-04-14 of 60M");
        when_I_list_all_sessions_between("2020-02-01", "2020-04-01");
        then_listed_sessions_should_be(
                "workshop on 2020-02-12 of 180M",
                "volunteer on 2020-03-04 of 180M");
    }

    private void given_sessions(final String... sessions) {
        this.repository = repository(sessions);
    }

    private void when_I_list_all_sessions() {
        sessions = AllSessions.build(this.repository).allSessions();
    }

    private void when_I_list_all_sessions_between(final String from, final String to) {
        sessions = AllSessions.build(this.repository).allSessionsBetween(LocalDate.parse(from), LocalDate.parse(to));
    }

    private void then_listed_sessions_should_be(final String... sessions) {
        assertEquals(List.of(sessions), this.sessions.map(renderer()::render));
    }
}
package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static eu.keilana.backend.session.domain.usecase.Helper.contentOf;
import static eu.keilana.backend.session.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("SameParameterValue")
class DeleteSessionShould {
    private SessionRepository repository = Default.inMemorySessions();

    @Test
    public void delete_an_existing_session() {
        given_sessions(
                "[a] workshop on 2020-02-12 of 180M",
                "[b] volunteer on 2020-03-04 of 180M",
                "[c] individual on 2020-04-14 of 60M");
        when_I_delete_session("b");
        then_sessions_should_be(
                "[a] workshop on 2020-02-12 of 180M",
                "[c] individual on 2020-04-14 of 60M");
    }

    private void given_sessions(final String... sessions) {
        this.repository = repository(sessions);
    }

    private void when_I_delete_session(final String identifier) {
        DeleteSession.build(this.repository).delete(Identifier.fromString(identifier));
    }

    private void then_sessions_should_be(final String... sessions) {
        assertEquals(List.of(sessions), contentOf(repository));
    }
}
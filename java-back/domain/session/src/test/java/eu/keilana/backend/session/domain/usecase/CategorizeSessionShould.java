package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.domain.model.Category;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static eu.keilana.backend.session.domain.usecase.Helper.contentOf;
import static eu.keilana.backend.session.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("SameParameterValue")
class CategorizeSessionShould {
    private SessionRepository repository = Default.inMemorySessions();

    @Test
    public void categorize_an_existing_session() {
        given_sessions(
                "[a] workshop on 2020-02-12 of 180M",
                "[b] volunteer on 2020-03-04 of 180M",
                "[c] individual on 2020-04-14 of 60M");
        when_I_categorize_session("b", "individual");
        then_sessions_should_be(
                "[a] workshop on 2020-02-12 of 180M",
                "[b] individual on 2020-03-04 of 180M",
                "[c] individual on 2020-04-14 of 60M");
    }

    private void given_sessions(final String... sessions) {
        this.repository = repository(sessions);
    }

    private void when_I_categorize_session(final String identifier, final String category) {
        CategorizeSession.build(this.repository).categorize(Identifier.fromString(identifier), Category.parse(category));
    }

    private void then_sessions_should_be(final String... sessions) {
        assertEquals(List.of(sessions), contentOf(repository));
    }
}
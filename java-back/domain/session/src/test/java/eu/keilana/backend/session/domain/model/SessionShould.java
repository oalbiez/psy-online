package eu.keilana.backend.session.domain.model;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.library.test.property.Generators;
import io.vavr.Tuple;
import io.vavr.test.Arbitrary;
import io.vavr.test.Gen;
import io.vavr.test.Property;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.vavr.test.Arbitrary.string;
import static io.vavr.test.Gen.choose;
import static io.vavr.test.Gen.frequency;
import static java.time.Duration.ofMinutes;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SessionShould {

    @Test
    public void have_a_default_duration_of_60_minutes() {
        assertEquals(ofMinutes(60), defaultSession().duration());
    }

    @Test
    public void have_a_default_category_of_individual() {
        Assertions.assertEquals(Category.INDIVIDUAL, defaultSession().category());
    }

    @Test
    public void be_cloneable_by_updating_identifier() {
        Property.def("session.clone")
                .forAll(Arbitrary.ofAll(identifier()))
                .suchThat(i -> i.equals(defaultSession().clone(i).identifier()))
                .check()
                .assertIsSatisfied();
    }

    @Test
    public void be_able_to_change_duration() {
        Property.def("session.during")
                .forAll(Arbitrary.ofAll(Generators.duration()))
                .suchThat(d -> d.equals(defaultSession().during(d).duration()))
                .check()
                .assertIsSatisfied();
    }

    @Test
    public void be_able_to_change_date() {
        Property.def("session.move")
                .forAll(Arbitrary.ofAll(Generators.localDate()))
                .suchThat(d -> d.equals(defaultSession().move(d).when()))
                .check()
                .assertIsSatisfied();

        Property.def("session.on(date)")
                .forAll(Arbitrary.ofAll(Generators.localDate()))
                .suchThat(d -> d.equals(defaultSession().on(d).when()))
                .check()
                .assertIsSatisfied();
    }

    @Test
    public void be_able_to_change_category() {
        Property.def("session.category")
                .forAll(Arbitrary.of(Category.values()))
                .suchThat(c -> c.equals(defaultSession().category(c).category()))
                .check()
                .assertIsSatisfied();
    }

    private Session defaultSession() {
        return Session.session();
    }

    private Gen<Identifier> identifier() {
        return string(
                frequency(
                        Tuple.of(1, choose('a', 'z')),
                        Tuple.of(1, choose('0', '9'))))
                .apply(8)
                .map(Identifier::fromString);
    }
}
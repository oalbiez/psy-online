package eu.keilana.backend.session.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.session.adapter.SessionDSL;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static eu.keilana.backend.session.domain.usecase.Helper.contentOf;
import static eu.keilana.backend.session.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("SameParameterValue")
class CreateSessionShould {
    private SessionRepository repository = Default.inMemorySessions();
    private Identifier.Generator identifiers = Identifier.generateUUID();

    @Test
    public void create_a_new_session() {
        given_sessions(
                "[a] workshop on 2020-02-12 of 180M");
        and_next_identifier_is("b");
        when_I_create_a_new_session("individual on 2020-04-14 of 60M");
        then_sessions_should_be(
                "[a] workshop on 2020-02-12 of 180M",
                "[b] individual on 2020-04-14 of 60M");
    }

    private void given_sessions(final String... sessions) {
        this.repository = repository(sessions);
    }

    private void and_next_identifier_is(final String id) {
        identifiers = () -> Identifier.fromString(id);
    }

    private void when_I_create_a_new_session(final String definition) {
        final var session = SessionDSL.parse(definition).getOrElseThrow(() -> new RuntimeException("invalid session definition: " + definition));
        CreateSession.build(repository, identifiers).create(session.when(), session.duration(), session.category());
    }

    private void then_sessions_should_be(final String... sessions) {
        assertEquals(List.of(sessions), contentOf(repository));
    }
}
package eu.keilana.backend.domain.type;

import io.vavr.collection.List;

import java.util.Arrays;

public final class CrudLoader {

    @SafeVarargs
    public static <I, T> CRUD<I, T> load(final CRUD<I, T> crud, final T... values) {
        Arrays.stream(values).forEach(crud::save);
        return crud;
    }

    public static <I, T> CRUD<I, T> load(final CRUD<I, T> crud, final List<T> values) {
        values.forEach(crud::save);
        return crud;
    }

    private CrudLoader() {
    }
}

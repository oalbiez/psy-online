package eu.keilana.backend.domain.type;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;
import java.util.UUID;
import java.util.function.Supplier;

public final class Identifier {
    private final String value;

    public interface Generator extends Supplier<Identifier> {
    }

    public static Generator generateUUID() {
        return Identifier::uuid;
    }

    public static Identifier fromString(@Nullable final String value) {
        if (value == null) {
            throw new IllegalArgumentException("identifier cannot be null");
        }
        return new Identifier(value);
    }

    public static Identifier uuid() {
        return fromString(UUID.randomUUID().toString());
    }

    private Identifier(@NonNull final String value) {
        this.value = value;
    }

    public String asString() {
        return value;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Identifier that = (Identifier) o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "Identifier{" + value + '}';
    }
}

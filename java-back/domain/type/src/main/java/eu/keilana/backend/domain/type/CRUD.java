package eu.keilana.backend.domain.type;

import io.vavr.collection.List;
import io.vavr.control.Option;

public interface CRUD<I, T> {
    List<T> all();

    Option<T> find(I identifier);

    T save(T session);

    boolean delete(I identifier);
}

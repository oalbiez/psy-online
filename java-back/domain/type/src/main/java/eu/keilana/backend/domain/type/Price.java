package eu.keilana.backend.domain.type;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.math.BigDecimal;
import java.util.Objects;

import static java.math.RoundingMode.HALF_UP;

public final class Price {
    private final BigDecimal value;

    public static Price parse(final String definition) {
        return of(new BigDecimal(definition));
    }

    public static Price zero() {
        return new Price(BigDecimal.ZERO);
    }

    public static Price of(final double value) {
        return of(BigDecimal.valueOf(value));
    }

    public static Price of(final BigDecimal value) {
        return new Price(value);
    }

    public static Price add(final Price left, final Price right) {
        return new Price(left.value.add(right.value));
    }

    private Price(final BigDecimal value) {
        this.value = value;
    }

    public double normalizedValue() {
        return value.setScale(2, HALF_UP).doubleValue();
    }

    public String render() {
        return value.setScale(2, HALF_UP).toPlainString();
    }

    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Price price = (Price) o;
        return Objects.equals(value, price.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "Price{" + value + '}';
    }
}

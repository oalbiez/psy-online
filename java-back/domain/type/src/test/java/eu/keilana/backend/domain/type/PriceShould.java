package eu.keilana.backend.domain.type;

import eu.keilana.backend.library.test.MonoidChecker;
import io.vavr.test.Gen;
import org.junit.jupiter.api.Test;

import static eu.keilana.backend.domain.type.Price.add;
import static eu.keilana.backend.library.test.MonoidChecker.checkMonoid;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PriceShould {
    @Test
    public void be_a_monoid_on_add_and_zero() {
        checkMonoid(new MonoidChecker.Definition<Price>() {
            public Gen<Price> element() {
                return random -> Price.of(random.nextDouble());
            }

            public Price zero() {
                return Price.zero();
            }

            public Price compose(final Price left, final Price right) {
                return add(left, right);
            }
        });
    }

    @Test
    public void have_two_digits_precision() {
        assertEquals(0.0, Price.of(0.001).normalizedValue());
    }

    @Test
    public void be_displayable() {
        assertEquals("Price{60.0}", Price.of(60.0).toString());
    }
}
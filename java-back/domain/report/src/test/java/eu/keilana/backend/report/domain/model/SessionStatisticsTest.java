package eu.keilana.backend.report.domain.model;

import eu.keilana.backend.library.test.MonoidChecker;
import io.vavr.collection.List;
import io.vavr.test.Arbitrary;
import io.vavr.test.Gen;
import io.vavr.test.Property;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static eu.keilana.backend.library.test.MonoidChecker.checkMonoid;
import static eu.keilana.backend.library.test.property.Generators.duration;
import static eu.keilana.backend.report.domain.model.Session.session;
import static eu.keilana.backend.report.domain.model.SessionStatistics.fromSessions;
import static io.vavr.test.Gen.choose;
import static java.time.Duration.ZERO;

class SessionStatisticsTest {

    @Test
    public void session_statistics_should_be_a_monoid_on_add_and_zero() {
        checkMonoid(new MonoidChecker.Definition<SessionStatistics>() {
            public Gen<SessionStatistics> element() {
                return random -> {
                    final var sessions = List.fill(
                            random.nextInt(100),
                            () -> session(
                                    LocalDate.EPOCH,
                                    duration().apply(random),
                                    choose(Category.class).apply(random)));
                    return fromSessions(sessions);
                };
            }

            public SessionStatistics zero() {
                return SessionStatistics.zero();
            }

            public SessionStatistics compose(final SessionStatistics left, final SessionStatistics right) {
                return SessionStatistics.add(left, right);
            }
        });
    }

    @Test
    public void zero_should_returns_null_duration_for_all_categories() {
        final var zero = SessionStatistics.zero();
        Property.def("zero should have all duration set to zero")
                .forAll(Arbitrary.of(Category.values()))
                .suchThat(c -> zero.of(c).equals(ZERO))
                .check()
                .assertIsSatisfied();
    }
}
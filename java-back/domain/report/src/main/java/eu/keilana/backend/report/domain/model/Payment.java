package eu.keilana.backend.report.domain.model;


import eu.keilana.backend.domain.type.Price;

import java.time.LocalDate;
import java.time.Month;


public final class Payment {
    private final LocalDate when;
    private final Price amount;

    public static Payment payment() {
        return new Payment(LocalDate.EPOCH, Price.zero());
    }

    public Payment move(final LocalDate when) {
        return new Payment(when, amount);
    }

    public Payment on(final int year, final Month month, final int dayOfMonth) {
        return new Payment(LocalDate.of(year, month, dayOfMonth), amount);
    }

    public Payment of(final Price amount) {
        return new Payment(when, amount);
    }

    private Payment(final LocalDate when, final Price amount) {
        this.when = when;
        this.amount = amount;
    }

    public LocalDate when() {
        return when;
    }

    public Price amount() {
        return amount;
    }
}

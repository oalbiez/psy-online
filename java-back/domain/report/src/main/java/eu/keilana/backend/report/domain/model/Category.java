package eu.keilana.backend.report.domain.model;

public enum Category {
    WORKSHOP, INDIVIDUAL, VOLUNTEER
}

package eu.keilana.backend.report.domain.model;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Duration;
import java.util.Objects;

import static java.time.Duration.ZERO;

public class SessionStatistics {
    private final Map<Category, Duration> data;

    public static SessionStatistics zero() {
        return new SessionStatistics(HashMap.empty());
    }

    public static SessionStatistics fromSessions(final List<Session> sessions) {
        return sessions.map(SessionStatistics::fromSession).fold(zero(), SessionStatistics::add);
    }

    public static SessionStatistics fromSession(final Session session) {
        return new SessionStatistics(HashMap.of(session.category(), session.duration()));
    }

    public static SessionStatistics add(final SessionStatistics left, final SessionStatistics right) {
        return new SessionStatistics(
                List
                        .of(Category.values())
                        .collect(HashMap.collector(c -> c, c -> left.of(c).plus(right.of(c)))));
    }

    private SessionStatistics(final Map<Category, Duration> data) {
        this.data = data;
    }

    public Duration of(final Category category) {
        return data.get(category).getOrElse(ZERO);
    }

    public Duration of(final Category... categories) {
        return HashSet.of(categories).map(this::of).fold(ZERO, Duration::plus);
    }

    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final SessionStatistics that = (SessionStatistics) o;
        return List
                .of(Category.values())
                .map(c -> of(c).equals(that.of(c)))
                .fold(true, (x, y) -> x && y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "SessionStatistics{" +
                "data=" + data +
                '}';
    }
}

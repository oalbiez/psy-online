package eu.keilana.backend.report.domain.usecase;

import eu.keilana.backend.report.domain.model.Session;
import io.vavr.collection.List;

import java.time.LocalDate;

public interface SessionProvider {
    List<Session> allBetween(final LocalDate from, LocalDate to);
}

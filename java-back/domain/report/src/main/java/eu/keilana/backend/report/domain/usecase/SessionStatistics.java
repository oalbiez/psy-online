package eu.keilana.backend.report.domain.usecase;

import java.time.LocalDate;

import static eu.keilana.backend.report.domain.model.SessionStatistics.fromSessions;

public final class SessionStatistics {
    private final SessionProvider sessions;

    public static SessionStatistics build(final SessionProvider sessions) {
        return new SessionStatistics(sessions);
    }

    private SessionStatistics(final SessionProvider sessions) {
        this.sessions = sessions;
    }

    public eu.keilana.backend.report.domain.model.SessionStatistics report(final LocalDate from, final LocalDate to) {
        return fromSessions(sessions.allBetween(from, to));
    }
}

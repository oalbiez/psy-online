package eu.keilana.backend.report.domain.model;

import java.time.Duration;
import java.time.LocalDate;

public class Session {
    private final LocalDate when;
    private final Duration duration;
    private final Category category;

    public static Session session(final LocalDate when, final Duration duration, final Category category) {
        return new Session(when, duration, category);
    }

    private Session(final LocalDate when, final Duration duration, final Category category) {
        this.when = when;
        this.duration = duration;
        this.category = category;
    }

    public LocalDate when() {
        return when;
    }

    public Duration duration() {
        return duration;
    }

    public Category category() {
        return category;
    }
}

package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.payment.domain.adapter.PaymentDSL;
import io.vavr.collection.List;
import io.vavr.control.Either;

import static eu.keilana.backend.domain.type.CrudLoader.load;

final class Helper {
    static PaymentRepository repository(final String... sessions) {
        final var repository = Default.inMemoryPayments();
        load(repository, List.of(sessions).map(PaymentDSL::parse).flatMap(Either::iterator));
        return repository;
    }

    static List<String> contentOf(final PaymentRepository repository) {
        return AllPayments.build(repository).allPayments().map(PaymentDSL.renderer().withIdentifier()::render);
    }

    private Helper() {
    }
}

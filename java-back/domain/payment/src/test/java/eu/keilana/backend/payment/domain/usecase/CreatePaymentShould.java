package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.payment.domain.adapter.PaymentDSL;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static eu.keilana.backend.payment.domain.usecase.Helper.contentOf;
import static eu.keilana.backend.payment.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("SameParameterValue")
class CreatePaymentShould {
    private PaymentRepository repository = Default.inMemoryPayments();
    private Identifier.Generator identifiers = Identifier.generateUUID();

    @Test
    public void create_a_new_payment() {
        given_payments(
                "[a] 2020-02-12 for 80.00");
        and_next_identifier_is("b");
        when_I_create_a_new_payment("2020-04-14 for 120.00");
        then_payments_should_be(
                "[a] 2020-02-12 for 80.00",
                "[b] 2020-04-14 for 120.00");
    }

    private void given_payments(final String... payments) {
        this.repository = repository(payments);
    }

    private void and_next_identifier_is(final String id) {
        identifiers = () -> Identifier.fromString(id);
    }

    private void when_I_create_a_new_payment(final String definition) {
        final var payment = PaymentDSL.parse(definition).getOrElseThrow(() -> new RuntimeException("invalid payment definition: " + definition));
        CreatePayment.build(repository, identifiers).create(payment.when(), payment.amount());
    }

    private void then_payments_should_be(final String... payments) {
        assertEquals(List.of(payments), contentOf(repository));
    }
}
package eu.keilana.backend.payment.domain.adapter;

import eu.keilana.backend.domain.type.Price;
import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.control.Either;
import io.vavr.test.Arbitrary;
import io.vavr.test.Gen;
import io.vavr.test.Property;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static eu.keilana.backend.library.test.property.Generators.localDate;
import static eu.keilana.backend.payment.domain.adapter.PaymentDSL.parse;
import static eu.keilana.backend.payment.domain.adapter.PaymentDSL.renderer;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_DOWN;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentDSLShould {

    @Test
    public void be_able_to_parse_and_render_with_identifier() {
        final var renderer = renderer().withIdentifier();
        Property.def("parse and render with identifier")
                .forAll(Arbitrary.ofAll(anyPayment()).map(renderer::render))
                .suchThat(s -> parse(s).map(renderer::render).equals(Either.right(s)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    public void be_able_to_parse_and_render_without_identifier() {
        Property.def("parse and render without identifier")
                .forAll(Arbitrary.ofAll(anyPayment()).map(renderer()::render))
                .suchThat(s -> parse(s).map(renderer()::render).equals(Either.right(s)))
                .check()
                .assertIsSatisfied();
    }

    @ParameterizedTest()
    @ValueSource(strings = {
            "#",
            " #",
            " # toto"
    })
    public void be_able_to_parse_with_comments(final String comment) {
        final var payment = "2020-02-12 for 80.00";
        assertEquals(Either.right(payment), parse(payment + comment).map(PaymentDSL.renderer()::render));
    }

    @Test
    public void be_able_reject_invalid_parsing() {
        assertEquals(Either.left("Failure[1:1]: digit expected"), parse("invalid"));
    }

    private static Gen<Payment> anyPayment() {
        return random -> Payment.payment()
                .on(localDate().apply(random))
                .of(Price.of(valueOf(random.nextInt(1_000_000)).divide(TEN, HALF_DOWN)));
    }
}
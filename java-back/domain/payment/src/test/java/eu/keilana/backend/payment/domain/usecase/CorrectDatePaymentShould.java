package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static eu.keilana.backend.payment.domain.usecase.Default.inMemoryPayments;
import static eu.keilana.backend.payment.domain.usecase.Helper.contentOf;
import static eu.keilana.backend.payment.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SuppressWarnings("SameParameterValue")
class CorrectDatePaymentShould {
    private PaymentRepository repository = inMemoryPayments();

    @Test
    public void correct_an_existing_payment() {
        given_payments(
                "[a] 2020-02-12 for 80.00",
                "[b] 2020-03-08 for 20.00",
                "[c] 2020-04-10 for 70.00");
        when_I_correct_payment("b", "2020-03-14");
        then_payments_should_be(
                "[a] 2020-02-12 for 80.00",
                "[b] 2020-03-14 for 20.00",
                "[c] 2020-04-10 for 70.00");
    }

    private void given_payments(final String... payments) {
        this.repository = repository(payments);
    }

    private void when_I_correct_payment(final String identifier, final String date) {
        CorrectDatePayment.build(this.repository).correct(Identifier.fromString(identifier), LocalDate.parse(date));
    }

    private void then_payments_should_be(final String... payments) {
        assertEquals(List.of(payments), contentOf(repository));
    }
}
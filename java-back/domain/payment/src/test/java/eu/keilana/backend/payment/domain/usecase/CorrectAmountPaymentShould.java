package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static eu.keilana.backend.payment.domain.usecase.Default.inMemoryPayments;
import static eu.keilana.backend.payment.domain.usecase.Helper.contentOf;
import static eu.keilana.backend.payment.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SuppressWarnings("SameParameterValue")
class CorrectAmountPaymentShould {
    private PaymentRepository repository = inMemoryPayments();

    @Test
    public void correct_an_existing_payment() {
        given_payments(
                "[a] 2020-02-12 for 80.00",
                "[b] 2020-03-08 for 20.00",
                "[c] 2020-04-10 for 70.00");
        when_I_correct_payment("b", "100.00");
        then_payments_should_be(
                "[a] 2020-02-12 for 80.00",
                "[b] 2020-03-08 for 100.00",
                "[c] 2020-04-10 for 70.00");
    }

    private void given_payments(final String... payments) {
        this.repository = repository(payments);
    }

    private void when_I_correct_payment(final String identifier, final String amount) {
        CorrectAmountPayment.build(this.repository).correct(Identifier.fromString(identifier), Price.parse(amount));
    }

    private void then_payments_should_be(final String... payments) {
        assertEquals(List.of(payments), contentOf(repository));
    }
}
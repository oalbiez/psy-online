package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static eu.keilana.backend.payment.domain.adapter.PaymentDSL.renderer;
import static eu.keilana.backend.payment.domain.usecase.Helper.repository;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("SameParameterValue")
class AllPaymentsShould {
    private PaymentRepository repository = Default.inMemoryPayments();
    private List<Payment> payments = List.empty();

    @Test
    public void list_all_payments_in_repository() {
        given_payments(
                "2020-02-12 for 80.00",
                "2020-03-08 for 20.00",
                "2020-04-10 for 70.00");
        when_I_list_all_payments();
        then_listed_payments_should_be(
                "2020-02-12 for 80.00",
                "2020-03-08 for 20.00",
                "2020-04-10 for 70.00");
    }

    @Test
    public void list_all_payments_in_repository_between_two_dates() {
        given_payments(
                "2020-02-12 for 80.00",
                "2020-03-08 for 20.00",
                "2020-04-10 for 70.00");
        when_I_list_all_payments_between("2020-02-01", "2020-04-01");
        then_listed_payments_should_be(
                "2020-02-12 for 80.00",
                "2020-03-08 for 20.00");
    }

    private void given_payments(final String... payments) {
        this.repository = repository(payments);
    }

    private void when_I_list_all_payments() {
        payments = AllPayments.build(this.repository).allPayments();
    }


    private void when_I_list_all_payments_between(final String from, final String to) {
        payments = AllPayments.build(this.repository).allPaymentsBetween(LocalDate.parse(from), LocalDate.parse(to));
    }

    private void then_listed_payments_should_be(final String... payments) {
        assertEquals(List.of(payments), this.payments.map(renderer()::render));
    }
}
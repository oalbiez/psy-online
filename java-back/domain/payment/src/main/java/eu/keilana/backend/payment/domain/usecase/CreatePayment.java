package eu.keilana.backend.payment.domain.usecase;


import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import eu.keilana.backend.payment.domain.model.Payment;

import java.time.LocalDate;

import static eu.keilana.backend.payment.domain.model.Payment.payment;


public final class CreatePayment {
    private final PaymentRepository payments;
    private final Identifier.Generator identifiers;

    public static CreatePayment build(final PaymentRepository payments, final Identifier.Generator identifiers) {
        return new CreatePayment(payments, identifiers);
    }

    private CreatePayment(final PaymentRepository payments, final Identifier.Generator identifiers) {
        this.payments = payments;
        this.identifiers = identifiers;
    }

    public Payment create(final LocalDate when, final Price amount) {
        return payments.save(payment(identifiers.get()).on(when).of(amount));
    }
}

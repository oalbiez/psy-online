package eu.keilana.backend.payment.domain.usecase;

import com.google.common.collect.Maps;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.collection.List;
import io.vavr.control.Option;

import java.time.LocalDate;
import java.util.Map;

public final class Default {

    public static PaymentRepository inMemoryPayments() {
        return new PaymentRepository() {
            private final Map<Identifier, Payment> payments = Maps.newHashMap();

            public List<Payment> all() {
                return List.ofAll(payments.values()).sortBy(Payment::when);
            }

            public List<Payment> allBetween(final LocalDate from, final LocalDate to) {
                return all().filter(s -> s.when().isAfter(from) && s.when().isBefore(to));
            }

            public Option<Payment> find(final Identifier identifier) {
                return Option.of(payments.get(identifier));
            }

            public Payment save(final Payment payment) {
                payments.put(payment.identifier(), payment);
                return payment;
            }

            public boolean delete(final Identifier identifier) {
                payments.remove(identifier);
                return true;
            }
        };
    }

    private Default() {
    }
}

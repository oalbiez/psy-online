package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.control.Option;

import java.util.function.Function;

final class PaymentUpdater {
    public static Option<Payment> update(final PaymentRepository repository, final Identifier identifier, final Function<Payment, Payment> action) {
        return repository.find(identifier)
                .map(action)
                .map(repository::save);
    }

    private PaymentUpdater() {
    }
}

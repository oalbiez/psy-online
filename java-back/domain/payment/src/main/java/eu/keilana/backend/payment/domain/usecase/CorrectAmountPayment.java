package eu.keilana.backend.payment.domain.usecase;


import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.control.Option;


public final class CorrectAmountPayment {
    private final PaymentRepository payments;

    public static CorrectAmountPayment build(final PaymentRepository payments) {
        return new CorrectAmountPayment(payments);
    }

    private CorrectAmountPayment(final PaymentRepository payments) {
        this.payments = payments;
    }

    public Option<Payment> correct(final Identifier identifier, final Price amount) {
        return PaymentUpdater.update(payments, identifier, p -> p.of(amount));
    }
}

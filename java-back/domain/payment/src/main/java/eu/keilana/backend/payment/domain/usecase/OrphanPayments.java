package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.collection.List;

import java.time.LocalDate;

public class OrphanPayments {
    private final PaymentRepository payments;

    public static OrphanPayments build(final PaymentRepository payments) {
        return new OrphanPayments(payments);
    }

    private OrphanPayments(final PaymentRepository payments) {
        this.payments = payments;
    }

    public List<Payment> allOrphanPayments() {
        return orphanedPayments(payments.all());
    }

    public List<Payment> allOrphanPaymentsBetween(final LocalDate from, final LocalDate to) {
        return orphanedPayments(payments.allBetween(from, to));
    }

    private static List<Payment> orphanedPayments(final List<Payment> payments) {
        return payments.filter(Payment::isOrphaned);
    }
}

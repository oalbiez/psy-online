package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.domain.type.CRUD;
import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.collection.List;

import java.time.LocalDate;

public interface PaymentRepository extends CRUD<Identifier, Payment> {
    List<Payment> allBetween(LocalDate from, LocalDate to);
}

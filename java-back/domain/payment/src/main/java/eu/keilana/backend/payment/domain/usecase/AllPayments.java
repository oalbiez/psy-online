package eu.keilana.backend.payment.domain.usecase;

import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.collection.List;

import java.time.LocalDate;

public final class AllPayments {
    private final PaymentRepository payments;

    public static AllPayments build(final PaymentRepository payments) {
        return new AllPayments(payments);
    }

    private AllPayments(final PaymentRepository payments) {
        this.payments = payments;
    }

    public List<Payment> allPayments() {
        return payments.all();
    }

    public List<Payment> allPaymentsBetween(final LocalDate from, final LocalDate to) {
        return payments.allBetween(from, to);
    }
}

package eu.keilana.backend.payment.domain.model;

import eu.keilana.backend.domain.type.Identifier;
import io.vavr.collection.List;

public class Sessions {
    private final List<Identifier> sessions;

    public static Sessions empty() {
        return new Sessions(List.empty());
    }

    public static Sessions sessions(final List<Identifier> sessions) {
        return new Sessions(sessions);
    }

    private Sessions(final List<Identifier> sessions) {
        this.sessions = sessions;
    }

    public boolean isEmpty() {
        return sessions.isEmpty();
    }
}

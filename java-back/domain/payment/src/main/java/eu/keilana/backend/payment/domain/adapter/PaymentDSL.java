package eu.keilana.backend.payment.domain.adapter;

import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.control.Either;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.petitparser.context.Token;

import java.time.LocalDate;

import static org.petitparser.parser.primitive.CharacterParser.*;
import static org.petitparser.parser.primitive.StringParser.ofIgnoringCase;

public final class PaymentDSL {

    public static Either<String, Payment> parse(final @NonNull String declaration) {
        final var id = of('[').seq(word().or(of('-')).plus().flatten().token(), of(']')).pick(1).trim();
        final var date = digit().repeat(4, 5).seq(of('-'), digit().times(2), of('-'), digit().times(2)).flatten().trim().token();
        final var amount = digit().plus().seq(of('.'), digit().times(2)).flatten().trim().token();

        final var builder = new PaymentBuilder();
        final var session = id.mapWithSideEffects(builder::identifier).optional()
                .seq(
                        date.mapWithSideEffects(builder::on),
                        ofIgnoringCase("for"),
                        amount.mapWithSideEffects(builder::amount),
                        of('#').seq(any().star()).optional())
                .trim().end();

        final var parse = session.parse(declaration);
        if (parse.isSuccess()) {
            return Either.right(builder.payment);
        }
        return Either.left(parse.toString());
    }

    public interface Renderer {
        Renderer withIdentifier();

        String render(final Payment session);
    }

    public static Renderer renderer() {
        return new Renderer() {
            private boolean withIdentifier = false;

            public Renderer withIdentifier() {
                withIdentifier = true;
                return this;
            }

            public String render(final Payment session) {
                if (withIdentifier) {
                    return "[" + session.identifier().asString() + "] " + internal(session);
                }
                return internal(session);
            }

            private String internal(final Payment session) {
                return session.when().toString() +
                        " for " +
                        session.amount().render();
            }
        };
    }


    private static class PaymentBuilder {
        Payment payment = Payment.payment();

        Token identifier(final Token token) {
            payment = payment.clone(Identifier.fromString(token.getValue()));
            return token;
        }

        Token on(final Token token) {
            payment = payment.on(LocalDate.parse(token.getValue()));
            return token;
        }

        Token amount(final Token token) {
            payment = payment.of(Price.parse(token.getValue()));
            return token;
        }
    }

    private PaymentDSL() {
    }
}

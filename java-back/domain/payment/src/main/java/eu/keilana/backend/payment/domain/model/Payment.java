package eu.keilana.backend.payment.domain.model;


import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.domain.type.Price;
import io.vavr.collection.List;

import java.time.LocalDate;

import static eu.keilana.backend.domain.type.Identifier.uuid;
import static java.util.Arrays.asList;


public final class Payment {
    private final Identifier identifier;
    private final LocalDate when;
    private final Price amount;
    final List<Identifier> relatedTo;

    public static Payment payment() {
        return payment(uuid());
    }

    public static Payment payment(final Identifier identifier) {
        return new Payment(identifier, LocalDate.EPOCH, Price.zero(), List.empty());
    }

    public Payment clone(final Identifier identifier) {
        return new Payment(identifier, when, amount, relatedTo);
    }

    public Payment on(final LocalDate when) {
        return new Payment(identifier, when, amount, relatedTo);
    }

    public Payment of(final Price amount) {
        return new Payment(identifier, when, amount, relatedTo);
    }

    public Payment relateTo(final Identifier... sessions) {
        return new Payment(identifier, when, amount, relatedTo.appendAll(asList(sessions)));
    }

    private Payment(final Identifier identifier, final LocalDate when, final Price amount, final List<Identifier> relatedTo) {
        this.identifier = identifier;
        this.when = when;
        this.amount = amount;
        this.relatedTo = relatedTo;
    }

    public Identifier identifier() {
        return identifier;
    }

    public LocalDate when() {
        return when;
    }

    public Price amount() {
        return amount;
    }

    public boolean isOrphaned() {
        return relatedTo.isEmpty();
    }

    @Override
    public String toString() {
        return "Payment{" +
                "identifier=" + identifier +
                ", when=" + when +
                ", amount=" + amount +
                ", relatedTo=" + relatedTo +
                '}';
    }
}

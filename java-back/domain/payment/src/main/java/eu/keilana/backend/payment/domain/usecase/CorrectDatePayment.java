package eu.keilana.backend.payment.domain.usecase;


import eu.keilana.backend.domain.type.Identifier;
import eu.keilana.backend.payment.domain.model.Payment;
import io.vavr.control.Option;

import java.time.LocalDate;


public final class CorrectDatePayment {
    private final PaymentRepository payments;

    public static CorrectDatePayment build(final PaymentRepository payments) {
        return new CorrectDatePayment(payments);
    }

    private CorrectDatePayment(final PaymentRepository payments) {
        this.payments = payments;
    }

    public Option<Payment> correct(final Identifier identifier, final LocalDate date) {
        return PaymentUpdater.update(payments, identifier, p -> p.on(date));
    }
}

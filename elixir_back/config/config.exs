# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :elixir_back,
  ecto_repos: [ElixirBack.Repo]

# Configures the endpoint
config :elixir_back, ElixirBackWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "lD+Qc2YdNufURGF+Mg+mrmJM0+RLP3vh4gum04Spm9NzdxzZBtI3PHth/G0eOMUk",
  render_errors: [view: ElixirBackWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ElixirBack.PubSub,
  live_view: [signing_salt: "Tlo7lle5"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

defmodule ElixirBackWeb.SessionController do
  use ElixirBackWeb, :controller

  def index(conn, _params) do
    json conn, ElixirBack.Repo.all(ElixirBack.Session)
  end
end

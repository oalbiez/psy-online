defmodule ElixirBackWeb.PageController do
  use ElixirBackWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end

defmodule ElixirBack.Session do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:category, :duration, :when]}
  schema "sessions" do
    field :category, :string
    field :duration, :integer
    field :when, :date

    timestamps()
  end



  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:when, :duration, :category])
    |> validate_required([:when, :duration, :category])
  end
end

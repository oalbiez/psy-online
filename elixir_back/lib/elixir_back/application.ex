defmodule ElixirBack.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      ElixirBack.Repo,
      # Start the Telemetry supervisor
      ElixirBackWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: ElixirBack.PubSub},
      # Start the Endpoint (http/https)
      ElixirBackWeb.Endpoint
      # Start a worker by calling: ElixirBack.Worker.start_link(arg)
      # {ElixirBack.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ElixirBack.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ElixirBackWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

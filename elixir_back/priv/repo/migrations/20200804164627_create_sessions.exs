defmodule ElixirBack.Repo.Migrations.CreateSessions do
  use Ecto.Migration

  def change do
    create table(:sessions) do
      add :when, :date
      add :duration, :integer
      add :category, :string

      timestamps()
    end

  end
end

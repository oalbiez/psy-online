# -*- coding: utf-8 -*-
import abc
import datetime

from typing import List, Callable
from business.session.domain.model import Session, Category
from business.identifier import Identifier


class SessionRepository(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def all(self) -> List[Session]:
        pass

    @abc.abstractmethod
    def find(self, identifier: Identifier) -> Session:
        pass

    @abc.abstractmethod
    def save(self, session: Session) -> Session:
        pass

    @abc.abstractmethod
    def delete(self, identifier: Identifier) -> bool:
        pass


IdentifierGenerator = Callable[[], Identifier]


class AllSessions:
    sessions: SessionRepository

    def all_sessions(self) -> List[Session]:
        return self.sessions.all()


class CreateSession:
    sessions: SessionRepository

    def create(
            self,
            idgen: IdentifierGenerator,
            when: datetime.datetime,
            duration: datetime.timedelta,
            category: Category) -> Session:
        return self.sessions.save(Session(idgen(), when, duration, category))


class DeleteSession:
    sessions: SessionRepository

    def delete(
            self,
            identifier: Identifier) -> bool:
        return self.sessions.delete(identifier)


class MoveSession:
    sessions: SessionRepository

    def move(
            self,
            identifier: Identifier,
            when: datetime.datetime) -> Session:
        return update(self.sessions, identifier, lambda s: s.move(when))


def update(
        sessions: SessionRepository,
        identifier: Identifier,
        _: Callable[[Session], Session]) -> Session:
    return sessions.find(identifier)


# moveSession :: SessionRepository r => r -> Identifier Session -> LocalTime -> IO (Maybe Session)
# moveSession repo sid when =
#     update repo sid (\s -> s {when = when})


# categorizeSession :: SessionRepository r => r -> Identifier Session -> Category -> IO (Maybe Session)
# categorizeSession repo sid category =
#     update repo sid (\s -> s {category = category})


# resizeSession :: SessionRepository r => r -> Identifier Session -> DiffTime -> IO (Maybe Session)
# resizeSession repo sid duration =
#     update repo sid (\s -> s {duration = duration})


# update :: SessionRepository r => r -> Identifier Session -> (Session -> Session) -> IO (Maybe Session)
# update repo sid f =
#     do
#         session <- find repo sid
#         fmap (save repo) session & sequence

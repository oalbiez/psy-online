# -*- coding: utf-8 -*-

import datetime
from dataclasses import dataclass
from adt import adt, Case
from business.identifier import Identifier
from utils.alias import alias, aliased


@adt
class Category:
    WORKSHOP: Case
    INDIVIDUAL: Case
    VOLUNTEER: Case


@aliased
@dataclass
class Session:
    identifier: Identifier
    when: datetime.datetime
    duration: datetime.timedelta
    category: Category

    def clone(self, identifier: Identifier) -> Session:
        return Session(identifier, self.when, self.duration, self.category)

    @alias("on")
    def move(self, when: datetime.datetime) -> Session:
        return Session(self.identifier, when, self.duration, self.category)

    def during(self, duration: datetime.timedelta) -> Session:
        return Session(self.identifier, self.when, duration, self.category)

    def categorize(self, category: Category) -> Session:
        return Session(self.identifier, self.when, self.duration, category)

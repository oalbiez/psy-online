# -*- coding: utf-8 -*-
from typing import NewType


Identifier = NewType('Identifier', str)

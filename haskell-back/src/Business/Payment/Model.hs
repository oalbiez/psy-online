{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
module Business.Payment.Model
  (
      Payment(..),
      Price(..)
  ) where


import           Business.Identifier
import           Data.Time.Clock
import           Data.Time.LocalTime
import           GHC.Generics
import           Money


type Price = Discrete "EUR" "euro"


data Payment = Payment
  {
    identifier :: Identifier Payment
  , when       :: LocalTime
  , price      :: Price
  } deriving (Show, Read, Generic)


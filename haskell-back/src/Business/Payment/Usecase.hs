module Business.Payment.Usecase where

import           Business.Identifier
import           Business.Payment.Model
import           Control.Monad.IO.Class
import           Data.Function          ((&))
import           Data.Time.Clock
import           Data.Time.LocalTime


class PaymentRepository repo where
    list :: repo -> IO [Payment]
    delete :: repo -> Identifier Payment -> IO Bool
    save :: repo -> Payment -> IO Payment
    find :: repo -> Identifier Payment -> IO (Maybe Payment)
    -- listBetween :: repo -> String -> String -> IO [Payment]


type IdGenerator = IO (Identifier Payment)


listPayment :: (MonadIO m, PaymentRepository r) => r -> m [Payment]
listPayment repo =
    liftIO $ list repo


getPayment :: (MonadIO m, PaymentRepository r) => r -> Identifier Payment -> m (Maybe Payment)
getPayment repo uuid =
    liftIO $ find repo uuid


createPayment :: (MonadIO m, PaymentRepository r) => r -> IdGenerator -> LocalTime -> Price -> m Payment
createPayment repo idgen when price =
    liftIO $ do
        id <- idgen
        save repo (Payment id when price)


deletePayment :: (MonadIO m, PaymentRepository r) => r -> Identifier Payment -> m Bool
deletePayment repo uuid =
    liftIO $ delete repo uuid


movePayment :: (MonadIO m, PaymentRepository r) => r -> Identifier Payment -> LocalTime -> m (Maybe Payment)
movePayment repo pid when =
    liftIO $ update repo pid (\s -> s {when = when})


modifyPayment :: (MonadIO m, PaymentRepository r) => r -> Identifier Payment -> Price -> m (Maybe Payment)
modifyPayment repo pid price =
    liftIO $ update repo pid (\s -> s {price = price})


update :: (MonadIO m, PaymentRepository r) => r -> Identifier Payment -> (Payment -> Payment) -> m (Maybe Payment)
update repo pid f =
    liftIO $ do
        payment <- find repo pid
        fmap (save repo) payment & sequence

module Business.Identifier
  ( Identifier,
    mkIdentifier,
    unIdentifier,
  ) where


import           Data.Function ((&))
import           Data.UUID
import           Data.UUID.V4


newtype Identifier a = Identifier String deriving (Show, Read, Eq)


mkIdentifier :: String -> Identifier a
mkIdentifier = Identifier


unIdentifier :: Identifier a -> String
unIdentifier (Identifier value) = value

{-# LANGUAGE DeriveGeneric #-}
module Business.Session.Model
  ( Session(..)
  , Category(..)
  , Duration(..)
  , minutes
  ) where


import           Business.Identifier
import           Data.Time.Clock
import           Data.Time.LocalTime
import           GHC.Generics

data Category
    = Workshop
    | Individual
    | Volunteer
    deriving (Show, Read, Eq, Generic)


newtype Duration
  = Duration Int
  deriving (Show, Read, Eq, Generic)


minutes :: Int -> Duration
minutes = Duration


data Session = Session
  {
    identifier :: Identifier Session
  , when       :: LocalTime
  , duration   :: Duration
  , category   :: Category
  } deriving (Show, Read, Generic)


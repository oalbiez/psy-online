module Business.Session.Usecase where

import           Business.Identifier
import           Business.Session.Model
import           Control.Monad.IO.Class
import           Data.Function          ((&))
import           Data.Time.Clock
import           Data.Time.LocalTime


-- Connection en type algebrique : Dummy | Postgres | File ...
-- passer la dependance en dur
-- utiliser un record


class SessionRepository repo where
    list :: repo -> IO [Session]
    delete :: repo -> Identifier Session -> IO Bool
    save :: repo -> Session -> IO Session
    find :: repo -> Identifier Session -> IO (Maybe Session)
    -- listBetween :: repo -> String -> String -> IO [Session]


type IdGenerator = IO (Identifier Session)


listSession :: (MonadIO m, SessionRepository r) => r -> m [Session]
listSession repo =
    liftIO $ list repo


getSession :: (MonadIO m, SessionRepository r) => r -> Identifier Session -> m (Maybe Session)
getSession repo uuid =
    liftIO $ find repo uuid


createSession :: (MonadIO m, SessionRepository r) => r -> IdGenerator -> LocalTime -> Duration -> Category -> m Session
createSession repo idgen when duration category =
    liftIO $ do
        id <- idgen
        save repo (Session id when duration category)


deleteSession :: (MonadIO m, SessionRepository r) => r -> Identifier Session -> m Bool
deleteSession repo uuid =
    liftIO $ delete repo uuid


moveSession :: (MonadIO m, SessionRepository r) => r -> Identifier Session -> LocalTime -> m (Maybe Session)
moveSession repo uuid when =
    liftIO $ update repo uuid (\s -> s {when = when})


categorizeSession :: (MonadIO m, SessionRepository r) => r -> Identifier Session -> Category -> m (Maybe Session)
categorizeSession repo uuid category =
    liftIO $ update repo uuid (\s -> s {category = category})


resizeSession :: (MonadIO m, SessionRepository r) => r -> Identifier Session -> Duration -> m (Maybe Session)
resizeSession repo uuid duration =
    liftIO $ update repo uuid (\s -> s {duration = duration})


update :: (MonadIO m, SessionRepository r) => r -> Identifier Session -> (Session -> Session) -> m (Maybe Session)
update repo uuid f =
    liftIO $ do
        session <- find repo uuid
        fmap (save repo) session & sequence

{-# LANGUAGE QuasiQuotes #-}
module Business.Session.Adapter
  ( RenderOption(..)
  , render
  ) where


import           Business.Session.Model
import           Data.String.Interpolate (i)


data RenderOption
  = WithIdentifier
  | WithoutIdentifier


render :: RenderOption -> Session -> String
render WithIdentifier session = [i|[#{identifier session}]|] ++ render WithoutIdentifier session
render _ session              = [i|#{category session} on #{when session} of #{duration session}|]

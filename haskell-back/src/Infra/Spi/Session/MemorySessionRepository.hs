module Infra.Spi.Session.MemorySessionRepository where

import           Business.Identifier
import           Business.Session.Model
import           Business.Session.Usecase
import           Data.Cache               as Cache
import           Data.Function            ((&))
import           Data.Time.Clock
import           Data.Time.LocalTime


newtype MemorySessionRepository = MemorySessionRepository
    { content :: Cache.Cache String Session
    }


mkMemorySessionRepository :: IO MemorySessionRepository
mkMemorySessionRepository =
    fmap MemorySessionRepository (Cache.newCache Nothing)


instance SessionRepository MemorySessionRepository
    where
        list repo = Cache.toList (content repo) & fmap (map $ \(a, b, c) -> b)
        delete repo sid = do
            Cache.delete (content repo) (unIdentifier sid)
            return True
        save repo session = do
            Cache.insert (content repo) (unIdentifier (identifier session)) session
            return session
        find repo sid = Cache.lookup (content repo) (unIdentifier sid)

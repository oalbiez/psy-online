module Infra.Spi.Session.FiledirSessionRepository where

import           Business.Identifier
import           Business.Session.Adapter
import           Business.Session.Model
import           Business.Session.Usecase
import           Data.Function            ((&))
import           Data.Maybe
import           Infra.Spi.FiledirStore   as Store


newtype Repository = Repository Store.FiledirStore


mkRepository :: FilePath -> Repository
mkRepository root = Repository (Store.mkStore root)


instance SessionRepository Repository
    where
        list (Repository store) = do
            contents <- Store.list store
            return (map decode contents & catMaybes)
        delete (Repository store) sid = do
            Store.delete store (unIdentifier sid)
            return True
        save (Repository store) session = do
            Store.put store (unIdentifier (identifier session)) (encode session)
            return session
        find (Repository store) sid = do
            content <- Store.get store (unIdentifier sid)
            return $ content >>= decode

encode :: Session -> String
encode = show

decode :: String -> Maybe Session
decode v = Just (read v)

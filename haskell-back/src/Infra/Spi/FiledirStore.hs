module Infra.Spi.FiledirStore
    (
        FiledirStore,
        delete,
        get,
        list,
        mkStore,
        put
    ) where

import           Control.Exception.Base
import           Control.Monad
import           Data.Maybe
import           System.Directory
import           System.IO.Error


newtype FiledirStore = FiledirStore FilePath


mkStore :: FilePath -> FiledirStore
mkStore = FiledirStore


list :: FiledirStore -> IO [String]
list (FiledirStore root) = do
    ensureRepo (FiledirStore root)
    files <- listDirectory root
    forM (map (\x -> root ++ "/" ++ x) files) readFile


put :: FiledirStore -> String -> String -> IO ()
put repo key value = do
    ensureRepo repo
    writeFile (pathForKey repo key) value


delete :: FiledirStore -> String -> IO ()
delete repo key = do
    ensureRepo repo
    removeFile (pathForKey repo key)


get :: FiledirStore -> String -> IO (Maybe String)
get repo key = do
    ensureRepo repo
    safeRead (pathForKey repo key)


pathForKey :: FiledirStore -> String -> FilePath
pathForKey (FiledirStore root) key = root ++ "/" ++ key


safeRead :: FilePath -> IO (Maybe String)
safeRead path = fmap Just (readFile path) `catch` handleExists
    where
        handleExists :: IOException -> IO (Maybe String)
        handleExists e
            | isDoesNotExistError e = return Nothing
            | otherwise = throw e

ensureRepo :: FiledirStore -> IO ()
ensureRepo (FiledirStore root) =
    createDirectoryIfMissing True root

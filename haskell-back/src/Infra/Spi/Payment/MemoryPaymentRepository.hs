module Infra.Spi.Payment.MemoryPaymentRepository where

import           Business.Identifier
import           Business.Payment.Model
import           Business.Payment.Usecase
import           Data.Cache               as Cache
import           Data.Function            ((&))
import           Data.Time.Clock
import           Data.Time.LocalTime


newtype MemoryPaymentRepository = MemoryPaymentRepository
    { content :: Cache.Cache String Payment
    }


mkMemoryPaymentRepository :: IO MemoryPaymentRepository
mkMemoryPaymentRepository =
    fmap MemoryPaymentRepository (Cache.newCache Nothing)


instance PaymentRepository MemoryPaymentRepository
    where
        list repo = Cache.toList (content repo) & fmap (map $ \(a, b, c) -> b)
        delete repo pid = do
            Cache.delete (content repo) (unIdentifier pid)
            return True
        save repo payment = do
            Cache.insert (content repo) (unIdentifier (identifier payment)) payment
            return payment
        find repo pid = Cache.lookup (content repo) (unIdentifier pid)

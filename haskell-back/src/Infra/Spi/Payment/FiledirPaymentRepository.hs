module Infra.Spi.Payment.FiledirPaymentRepository where

import           Business.Identifier
import           Business.Payment.Model
import           Business.Payment.Usecase
import           Data.Function            ((&))
import           Data.Maybe
import           Infra.Spi.FiledirStore   as Store


newtype Repository = Repository Store.FiledirStore


mkRepository :: FilePath -> Repository
mkRepository root = Repository (Store.mkStore root)


instance PaymentRepository Repository
    where
        list (Repository store) = do
            contents <- Store.list store
            return (map decode contents & catMaybes)
        delete (Repository store) pid = do
            Store.delete store (unIdentifier pid)
            return True
        save (Repository store) payment = do
            Store.put store (unIdentifier (identifier payment)) (encode payment)
            return payment
        find (Repository store) pid = do
            content <- Store.get store (unIdentifier pid)
            return $ content >>= decode

encode :: Payment -> String
encode = show

decode :: String -> Maybe Payment
decode v = Just (read v)

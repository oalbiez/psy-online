module Infra.Spi.IdentifierGenerator
  ( uuid
  ) where


import           Business.Identifier
import           Data.Function       ((&))
import           Data.UUID
import           Data.UUID.V4


uuid :: IO (Identifier a)
uuid =  mkIdentifier . toString <$> nextRandom

#!/bin/bash

SERVER=http://localhost:8090

function json {
    curl -X POST -H "Content-Type: application/json" -d "$2" $SERVER/$1
}

json session '{"when":"2020-10-04T00:32:46","duration":3600,"category":"individual"}'

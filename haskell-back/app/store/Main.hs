module Main where

import           Business.Identifier
import           Business.Session.Model
import           Business.Session.Usecase                   as Usecase
import           Data.Cache
import           Data.Time.Clock
import           Data.Time.LocalTime
import           Infra.Spi.IdentifierGenerator
import           Infra.Spi.Session.FiledirSessionRepository as Spi

main :: IO ()
main = do
  let store =  Spi.mkRepository "/tmp/polop"

  Usecase.createSession store uuid (read "2020-10-04 00:32:46.0") (minutes 60) Individual
  Usecase.createSession store uuid (read "2020-10-07 00:32:46.0") (minutes 90) Individual

  v <- Usecase.listSession store
  print v

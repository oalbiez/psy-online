{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}
module Main where

import           Business.Identifier
import           Business.Session.Model                     as Model
import           Business.Session.Usecase                   as Usecase
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Cache
import           Data.Function                              ((&))
import           Data.Proxy                                 (Proxy (Proxy))
import           Data.Text                                  (Text, pack)
import           Data.Time.Clock
import           Data.Time.LocalTime
import           GHC.Generics
import           Infra.Spi.IdentifierGenerator
import           Infra.Spi.Payment.FiledirPaymentRepository as PaymentStore
import           Infra.Spi.Session.FiledirSessionRepository as SessionStore
import           Network.Wai.Handler.Warp
import           PaymentServer
import           Servant
import           Servant.Server                             (Server, serve)
import           SessionServer


type ServerApi
  =    SessionApi
  :<|> PaymentApi


main :: IO ()
main = do
  let sessionStore = SessionStore.mkRepository "/tmp/db/session"
  let paymentStore = PaymentStore.mkRepository "/tmp/db/payment"
  let server = sessionServer sessionStore :<|> paymentServer paymentStore

  putStrLn "Local:  http://localhost:8090/"
  serve (Proxy :: Proxy ServerApi) server & run 8090

{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}
module SessionServer (SessionApi, sessionServer) where


import           Business.Identifier
import           Business.Session.Model                    as Model
import           Business.Session.Usecase                  as Usecase
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Cache
import           Data.Function                             ((&))
import           Data.Proxy                                (Proxy (Proxy))
import           Data.Text                                 (Text, pack, unpack)
import           Data.Time.Clock
import           Data.Time.LocalTime
import           GHC.Generics
import           Infra.Spi.IdentifierGenerator             (uuid)
import           Infra.Spi.Session.MemorySessionRepository as Spi
import           Network.Wai.Handler.Warp
import           Servant
import           Servant.Server                            (Server, serve)
import           Web.HttpApiData


instance FromJSON Category
instance FromJSON Duration

instance ToJSON Session where
  toJSON session =
    object
     [ "identifier" .= unIdentifier (Model.identifier session)
     , "when" .= Model.when session
     , "duration" .= show (Model.duration session)
     , "category" .= show (Model.category session) ]


data SessionCreation = SessionCreation
  {
    when     :: LocalTime
  , duration :: Model.Duration
  , category :: Category
  } deriving (Generic)

instance FromJSON SessionCreation


newtype SessionMove = SessionMove
  {
    newwhen     :: LocalTime
  } deriving (Generic)

instance FromJSON SessionMove


newtype SessionResize = SessionResize
  {
    newduration :: Model.Duration
  } deriving (Generic)

instance FromJSON SessionResize


newtype SessionCategorize = SessionCategorize
  {
    newcategory :: Category
  } deriving (Generic)

instance FromJSON SessionCategorize


instance FromHttpApiData (Identifier a) where
  parseUrlPiece value = Right $ mkIdentifier (unpack value)


type SessionApi
  =    "session" :> Get '[JSON] [Model.Session]
  :<|> "session" :> Capture "id" (Identifier Model.Session) :> Get '[JSON] (Maybe Model.Session)
  :<|> "session" :> Capture "id" (Identifier Model.Session) :> Delete '[JSON] Bool
  :<|> "session" :> ReqBody '[JSON] SessionCreation :> Post '[JSON] Model.Session
  :<|> "session" :> Capture "id" (Identifier Model.Session) :> "when" :> ReqBody '[JSON] SessionMove :> Post '[JSON] (Maybe Model.Session)
  :<|> "session" :> Capture "id" (Identifier Model.Session) :> "duration" :> ReqBody '[JSON] SessionResize :> Post '[JSON] (Maybe Model.Session)
  :<|> "session" :> Capture "id" (Identifier Model.Session) :> "category" :> ReqBody '[JSON] SessionCategorize :> Post '[JSON] (Maybe Model.Session)


sessionServer :: Usecase.SessionRepository r => r -> Server SessionApi
sessionServer store =
  sessionList
  :<|> sessionGet
  :<|> sessionDelete
  :<|> sessionCreate
  :<|> sessionMove
  :<|> sessionResize
  :<|> sessionCategorize
  where
    sessionList = Usecase.listSession store
    sessionGet = Usecase.getSession store
    sessionDelete = Usecase.deleteSession store
    sessionCreate content =
      Usecase.createSession store uuid when duration category
        where
          when = SessionServer.when content
          duration = SessionServer.duration content
          category = SessionServer.category content
    sessionMove uuid content =
      SessionServer.newwhen content & Usecase.moveSession store uuid
    sessionResize uuid content =
      SessionServer.newduration content & Usecase.resizeSession store uuid
    sessionCategorize uuid content =
      SessionServer.newcategory content & Usecase.categorizeSession store uuid

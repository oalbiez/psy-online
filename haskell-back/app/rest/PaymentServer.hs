{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}
module PaymentServer (PaymentApi, paymentServer) where


import           Business.Identifier
import           Business.Payment.Model                    as Model
import           Business.Payment.Usecase                  as Usecase
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Cache
import           Data.Function                             ((&))
import           Data.Proxy                                (Proxy (Proxy))
import           Data.Text                                 (Text, pack, unpack)
import           Data.Time.Clock
import           Data.Time.LocalTime
import           GHC.Generics
import           Infra.Spi.IdentifierGenerator             (uuid)
import           Infra.Spi.Payment.MemoryPaymentRepository as Spi
import           Money.Aeson
import           Network.Wai.Handler.Warp
import           Servant
import           Servant.Server                            (Server, serve)
import           Web.HttpApiData


instance ToJSON Payment where
  toJSON payment =
    object
     [ "identifier" .= unIdentifier (Model.identifier payment)
     , "when" .= Model.when payment
     , "price" .= show (Model.price payment) ]


data PaymentCreation = PaymentCreation
  {
    when  :: LocalTime
  , price :: Price
  } deriving (Generic)

instance FromJSON PaymentCreation


newtype PaymentMove = PaymentMove
  {
    newwhen     :: LocalTime
  } deriving (Generic)

instance FromJSON PaymentMove


newtype PaymentPrice = PaymentPrice
  {
    newprice :: Price
  } deriving (Generic)

instance FromJSON PaymentPrice


type PaymentApi
  =    "payment" :> Get '[JSON] [Model.Payment]
  :<|> "payment" :> Capture "id" (Identifier Model.Payment) :> Get '[JSON] (Maybe Model.Payment)
  :<|> "payment" :> Capture "id" (Identifier Model.Payment) :> Delete '[JSON] Bool
  :<|> "payment" :> ReqBody '[JSON] PaymentCreation :> Post '[JSON] Model.Payment
  :<|> "payment" :> Capture "id" (Identifier Model.Payment) :> "when" :> ReqBody '[JSON] PaymentMove :> Post '[JSON] (Maybe Model.Payment)
  :<|> "payment" :> Capture "id" (Identifier Model.Payment) :> "price" :> ReqBody '[JSON] PaymentPrice :> Post '[JSON] (Maybe Model.Payment)


paymentServer :: Usecase.PaymentRepository r => r -> Server PaymentApi
paymentServer store =
  paymentList
  :<|> paymentGet
  :<|> paymentDelete
  :<|> paymentCreate
  :<|> paymentMove
  :<|> paymentPrice
  where
    paymentList = Usecase.listPayment store
    paymentGet = Usecase.getPayment store
    paymentDelete = Usecase.deletePayment store
    paymentCreate content =
      Usecase.createPayment store uuid when price
        where
          when = PaymentServer.when content
          price = PaymentServer.price content
    paymentMove uuid content =
      PaymentServer.newwhen content & Usecase.movePayment store uuid
    paymentPrice uuid content =
      PaymentServer.newprice content & Usecase.modifyPayment store uuid

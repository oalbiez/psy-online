# TODO

- Corriger le logo dans la barre de navigation pour en faire une entrée de menu comme les autres
- Faire que le logo dans la barre de navigation envoie sur home
- Rajouter des illustrations

- cherhcer les infos de facturations chez stripe
- ajouter dans le paiement les metadata de facturation (durée de la séance)



# Etudes


## Dans les steps du paiement

Avoir des steps intermediaires dans le payment (#payment/session/1, #payment/session/2, #payment/session/3), comment revenir en arrière et stocker les données ?


## Avoir un meilleur routage des urls


## Le temps de travail n'est pas pris en compte pour le moment dans le paiement


## Autres moyens de paiements

- paypal
- mollie


## Revoir la page du bilan

    Mois       | Horaires    | Revenus
    ----------------------------------
    mars 2020  |  12H        |   340€
               |  4H benevol |
    ----------------------------------
    avril 2020 |  23H        |   340€
               |  6H benevol |

    + Liste des derniers paiements

## Statistiques

- Paiement horaire moyen
- Plus gros paiement
- Revenus mensuel souhaité


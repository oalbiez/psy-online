module Business.PriceTests exposing (all)

import Business.Price as Price
import Expect
import Fuzz exposing (Fuzzer, float)
import Random
import Shrink
import Test exposing (Test, describe, fuzz, fuzz2, test)


price : Fuzzer Price.Price
price =
    Fuzz.custom
        (Random.map Price.fromFloat (Random.float -10.0 1000.0))
        (\(Price.Price amount) -> Shrink.map Price.Price (Shrink.float amount))


all : Test
all =
    describe "Price" <|
        [ describe "Construction and rendering"
            [ test "zero" <|
                \_ ->
                    Price.zero
                        |> Price.toString
                        |> Expect.equal "0,00€"
            , test "fromInt" <|
                \_ ->
                    Price.fromInt 1000
                        |> Price.toString
                        |> Expect.equal "10,00€"
            , test "fromFloat" <|
                \_ ->
                    Price.fromFloat 10.0
                        |> Price.toString
                        |> Expect.equal "10,00€"
            , test "fromString" <|
                \_ ->
                    Price.fromString "10.00€"
                        |> Expect.equal (Price.fromFloat 10.0 |> Just)
            ]
        , describe "Rendering"
            [ fuzz price "Rendering should always finish with €" <|
                \p ->
                    Price.toString p
                        |> String.right 1
                        |> Expect.equal "€"
            ]
        , describe "Amount"
            [ fuzz float "Amount should return the price in float" <|
                \x ->
                    Price.fromFloat x
                        |> Price.amount
                        |> Expect.within (Expect.Absolute 0.001) x
            ]
        , describe "Price math"
            [ fuzz2 float float "Addition" <|
                \x y ->
                    Price.add (Price.fromFloat x) (Price.fromFloat y)
                        |> Expect.equal (Price.fromFloat (x + y))
            , fuzz2 float float "Substraction" <|
                \x y ->
                    Price.sub (Price.fromFloat x) (Price.fromFloat y)
                        |> Expect.equal (Price.fromFloat (x - y))
            , fuzz2 float float "Multiplication" <|
                \factor p ->
                    Price.mul factor (Price.fromFloat p)
                        |> Expect.equal (Price.fromFloat (factor * p))
            , fuzz float "Division" <|
                \p ->
                    Price.div 2.0 (Price.fromFloat p)
                        |> Expect.equal (Price.fromFloat (p / 2.0))
            , test "Sum" <|
                \_ ->
                    Price.sum [ Price.fromFloat 10.0, Price.fromFloat 15.0, Price.fromFloat 2.5 ]
                        |> Expect.equal (Price.fromFloat 27.5)
            ]
        ]

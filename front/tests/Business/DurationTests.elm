module Business.DurationTests exposing (all)

import Business.Duration as Duration
import Expect
import Fuzz exposing (Fuzzer, int)
import Random
import Shrink
import Test exposing (Test, describe, fuzz, fuzz2, test)


duration : Fuzzer Duration.Duration
duration =
    Fuzz.custom
        (Random.map Duration.minutes (Random.int 0 10000))
        (\(Duration.Duration value) -> Shrink.map Duration.Duration (Shrink.int value))


all : Test
all =
    describe "Duration" <|
        [ describe "Construction and rendering"
            [ test "zero" <|
                \_ ->
                    Duration.zero
                        |> Duration.toString
                        |> Expect.equal "00:00"
            , test "minutes" <|
                \_ ->
                    Duration.minutes 120
                        |> Duration.toString
                        |> Expect.equal "02:00"
            , test "hours" <|
                \_ ->
                    Duration.hours 2.5
                        |> Duration.toString
                        |> Expect.equal "02:30"
            ]
        , describe "Rendering"
            [ fuzz duration "Rendering should always ':' between hours and minutes" <|
                \d ->
                    Duration.toString d
                        |> String.right 3
                        |> String.startsWith ":"
                        |> Expect.equal True
            ]
        , describe "Conversions"
            [ fuzz int "toMinutes should return the duration in minutes" <|
                \x ->
                    Duration.minutes x
                        |> Duration.toMinutes
                        |> Expect.equal x
            , test "tohours should return the duration in hours" <|
                \_ ->
                    Duration.minutes 30
                        |> Duration.toHours
                        |> Expect.within (Expect.Absolute 0.001) 0.5
            ]
        , describe "Duration math"
            [ fuzz2 int int "Addition" <|
                \x y ->
                    Duration.add (Duration.minutes x) (Duration.minutes y)
                        |> Expect.equal (Duration.minutes (x + y))
            , test "Sum" <|
                \_ ->
                    Duration.sum [ Duration.minutes 10, Duration.minutes 15, Duration.minutes 2 ]
                        |> Expect.equal (Duration.minutes 27)
            ]
        ]

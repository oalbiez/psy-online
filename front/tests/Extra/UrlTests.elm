module Extra.UrlTests exposing (all)

import Expect
import Extra.Url exposing (useFragment, withFragment)
import Test exposing (Test, describe, test)
import Url


url : String -> Url.Url
url =
    Url.fromString
        >> Maybe.withDefault
            { protocol = Url.Https
            , host = ""
            , port_ = Nothing
            , path = ""
            , query = Nothing
            , fragment = Nothing
            }


all : Test
all =
    describe "Extra.Url"
        [ describe "useFragment should replace path with fragment"
            [ test "case with no path" <|
                \_ ->
                    url "https://keilana.albiez.eu/#first/second"
                        |> useFragment
                        |> Expect.equal (url "https://keilana.albiez.eu/first/second")
            , test "case with path" <|
                \_ ->
                    url "https://keilana.albiez.eu/ignored/#first/second"
                        |> useFragment
                        |> Expect.equal (url "https://keilana.albiez.eu/first/second")
            , test "case with no fragment" <|
                \_ ->
                    url "https://keilana.albiez.eu/ignored"
                        |> useFragment
                        |> Expect.equal (url "https://keilana.albiez.eu/")
            ]
        , describe "useFragment should add fragment to path"
            [ test "case with no path" <|
                \_ ->
                    url "https://keilana.albiez.eu/#first/second"
                        |> withFragment
                        |> Expect.equal (url "https://keilana.albiez.eu/first/second")
            , test "case with path" <|
                \_ ->
                    url "https://keilana.albiez.eu/before/#first/second"
                        |> withFragment
                        |> Expect.equal (url "https://keilana.albiez.eu/before/first/second")
            , test "case with no fragment" <|
                \_ ->
                    url "https://keilana.albiez.eu/before"
                        |> withFragment
                        |> Expect.equal (url "https://keilana.albiez.eu/before")
            ]
        ]

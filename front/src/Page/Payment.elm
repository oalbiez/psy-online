module Page.Payment exposing
    ( Model
    , Msg
    , cancel
    , init
    , paymentSessionView
    , paymentSupportView
    , paymentView
    , paymentWorkshopView
    , success
    , update
    )

import Browser
import Browser.Navigation as Nav
import Business.Duration as Duration
import Business.Price as Price
import Business.Statistics as Statistics
import Business.Work as Work
import Cmd.Extra exposing (withCmd, withNoCmd)
import Element exposing (Element, alignTop, centerX, column, el, fill, height, padding, paragraph, px, row, spacing, text, width)
import Element.Font as Font
import Element.Input as Input
import Extra.Url
import Lib.Stripe as Stripe
import Maybe
import Page.Partial.Layout as Layout
import Page.Partial.ReportTable as ReportTable
import Style.Button as Button
import Style.Font exposing (fa)
import Style.Step as Step
import Style.Theme as Theme
import Style.Typography as Typography
import Url
import Url.Parser exposing ((</>), map, oneOf, parse, s)
import Validate



---- MODEL ----


type alias Model =
    { key : Nav.Key
    , step : Int
    , duration : Maybe Duration.Duration
    , amount : String
    , product : Maybe Work.Category
    }


init : Url.Url -> Nav.Key -> Model
init url key =
    { key = key
    , step = 0
    , duration = Nothing
    , amount = ""
    , product = productFromUrl url
    }


productFromUrl : Url.Url -> Maybe Work.Category
productFromUrl =
    let
        parser =
            oneOf
                [ map Work.Individual (s "payment" </> s "session")
                , map Work.Workshop (s "payment" </> s "workshop")
                , map Work.Volunteer (s "payment" </> s "support")
                ]
    in
    Extra.Url.withFragment >> parse parser


resetPayment : Model -> Model
resetPayment model =
    { model
        | step = 0
        , duration = Nothing
        , amount = ""
    }


amount : Model -> Price.Price
amount model =
    model.amount |> String.toFloat |> Maybe.withDefault 0.0 |> Price.fromFloat



---- UPDATE ----


type Msg
    = SetProduct Work.Category String
    | NewAmount String
    | SetDuration Duration.Duration
    | SetAmount
    | QuickAmount String
    | Pay


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetProduct category url ->
            { model | product = Just category }
                |> resetPayment
                |> withCmd (Nav.pushUrl model.key url)

        NewAmount value ->
            { model | amount = value }
                |> withNoCmd

        SetDuration value ->
            { model | duration = Just value, step = model.step + 1 }
                |> withNoCmd

        SetAmount ->
            { model | step = model.step + 1 }
                |> withNoCmd

        QuickAmount value ->
            { model
                | amount = value
                , step = model.step + 1
            }
                |> withNoCmd

        Pay ->
            model |> withCmd (Stripe.clientCheckout (toProduct model))


toProduct : Model -> Stripe.ClientCheckout
toProduct model =
    let
        priceRef category =
            case category of
                Work.Individual ->
                    Stripe.PriceRef "price_1GtfAwLjunRWsLP873CDgex2"

                Work.Workshop ->
                    Stripe.PriceRef "price_1GtfwrLjunRWsLP8U5daBmWx"

                Work.Volunteer ->
                    Stripe.PriceRef "price_1GtfycLjunRWsLP8O34OD6aR"

        quantity =
            model.amount |> String.toFloat |> Maybe.withDefault 0.0 |> floor
    in
    { price = model.product |> Maybe.map priceRef |> Maybe.withDefault (Stripe.PriceRef "")
    , quantity = quantity
    }



---- STEPS ----


durationStep : String -> Step.Step Model Msg
durationStep position =
    { past = position |> renderDuration
    , current = position |> askDuration
    , futur = \_ -> Element.none
    }


renderDuration : String -> Model -> Element Msg
renderDuration position model =
    let
        duration =
            model.duration
                |> Maybe.map Duration.toString
                |> Maybe.withDefault "00:00"
    in
    [ "Durée de l'entretien: " |> text
    , duration |> text |> el [ Font.bold ]
    ]
        |> paragraph []
        |> Step.stepView Theme.secondary position


askDuration : String -> Model -> Element Msg
askDuration position _ =
    let
        button label value =
            Button.button [ width fill, Font.center ]
                { onPress = Just (SetDuration value)
                , label = label |> text
                , color = Theme.primary
                }
    in
    [ "Quelle durée avez-vous passé en entretien ?" |> text |> Typography.h4 []
    , 30 |> Duration.minutes |> button "30 minutes"
    , 60 |> Duration.minutes |> button "1 heure"
    , 90 |> Duration.minutes |> button "1 heure et demi"
    , 120 |> Duration.minutes |> button "2 heures"
    , 150 |> Duration.minutes |> button "2 heures et demi"
    , 180 |> Duration.minutes |> button "3 heures"
    ]
        |> column [ spacing 30 ]
        |> Step.stepView Theme.primary position


amountStep : Statistics.Statistics -> String -> Step.Step Model Msg
amountStep statistics position =
    { past = position |> renderAmount
    , current = position |> askAmount statistics
    , futur = \_ -> Element.none
    }


renderAmount : String -> Model -> Element Msg
renderAmount position model =
    [ "Montant à payer: " |> text
    , model
        |> amount
        |> Price.toString
        |> text
        |> el [ Font.bold ]
    ]
        |> paragraph []
        |> Step.stepView Theme.secondary position


askAmount : Statistics.Statistics -> String -> Model -> Element Msg
askAmount statistics position model =
    let
        amountValidator =
            Validate.firstError
                [ Validate.ifBlank .amount "Merci de renseigner un montant."
                , Validate.ifNotInt .amount (\_ -> "Le montant doit être un nombre entier.")
                ]

        validation =
            Validate.validate amountValidator model

        hasErrors =
            case validation of
                Ok _ ->
                    False

                _ ->
                    True

        shortcut value theme =
            Button.button [ alignTop, width fill, Font.center ]
                { onPress = QuickAmount value |> Just
                , label = value ++ "€" |> text
                , color = theme
                }
    in
    [ "Quel montant souhaitez vous payer ?" |> text |> Typography.h4 []
    , row [ width fill, spacing 10 ]
        [ shortcut "20" Theme.info
        , shortcut "40" Theme.primary
        , shortcut "60" Theme.info
        , shortcut "80" Theme.info
        , shortcut "100" Theme.info
        ]
    , column [ width fill ]
        [ row [ spacing 20, width fill ]
            [ Input.text [ Input.focusedOnLoad, alignTop ]
                { onChange = NewAmount
                , text = model.amount
                , placeholder = Nothing
                , label = "Montant en €" |> Input.labelHidden
                }
            , if hasErrors then
                Button.button [ alignTop, height fill ]
                    { onPress = Nothing
                    , label = "Valider" |> text
                    , color = Theme.secondary
                    }

              else
                Button.button [ alignTop, height fill ]
                    { onPress = SetAmount |> Just
                    , label = "Valider" |> text
                    , color = Theme.primary
                    }
            ]
        , case validation of
            Ok _ ->
                Element.none

            Err messages ->
                messages
                    |> List.map text
                    |> paragraph
                        [ padding 5
                        , Typography.scale -1
                        , Font.color Theme.danger.background
                        , width Element.shrink
                        ]
        ]
    , column [ spacing 10, Typography.scale -1, width fill ]
        [ "Bilans" |> text |> Typography.h5 []
        , ReportTable.monthlySummaryTable (ReportTable.Lines 6) statistics.payments statistics.work
        ]
    ]
        |> column [ spacing 10 ]
        |> Step.stepView Theme.primary position


payStep : String -> Step.Step Model Msg
payStep position =
    { past = \_ -> Element.none
    , current = position |> askPay
    , futur = \_ -> Element.none
    }


askPay : String -> Model -> Element Msg
askPay position _ =
    [ "Procédons au paiement" |> text |> Typography.h4 []
    , row [ spacing 30 ]
        [ Button.button [ Font.center, width (px 150) ]
            { onPress = Just Pay
            , label = [ fa [] "\u{F1F5}", " stripe" |> text ] |> paragraph []
            , color = Theme.primary
            }
        ]
    ]
        |> column [ spacing 30 ]
        |> Step.stepView Theme.primary position



---- VIEW ----


paymentView : Browser.Document Msg
paymentView =
    let
        button label msg =
            Button.button [ width fill, Font.center ]
                { onPress = Just msg
                , label = label |> text
                , color = Theme.primary
                }
    in
    [ button "Je souhaite payer pour une séance individuelle" (SetProduct Work.Individual "/#payment/session")
    , button "Je souhaite payer pour un atelier" (SetProduct Work.Workshop "/#payment/workshop")
    , button "Je souhaite soutenir" (SetProduct Work.Volunteer "/#payment/support")
    ]
        |> column [ spacing 30, centerX ]
        |> paymentLayout "Paiement"


paymentSessionView : Model -> Statistics.Statistics -> Browser.Document Msg
paymentSessionView model statistics =
    let
        workflow =
            [ durationStep "1", amountStep statistics "2", payStep "3" ]
    in
    [ [ "Je pratique un tarif libre. Vous pouvez me donner de l'argent pour soutenir mon activité professionelle" |> text ] |> paragraph []
    , Step.view (column [ spacing 50, width fill ]) workflow model model.step
    ]
        |> column [ spacing 30, width fill, Font.justify ]
        |> paymentLayout "Paiement d'une session"


paymentWorkshopView : Model -> Statistics.Statistics -> Browser.Document Msg
paymentWorkshopView model statistics =
    let
        workflow =
            [ amountStep statistics "1", payStep "2" ]
    in
    [ [ "Je pratique un tarif libre. Vous pouvez me donner de l'argent pour soutenir mon activité professionelle" |> text ] |> paragraph []
    , Step.view (column [ spacing 50, width fill ]) workflow model model.step
    ]
        |> column [ spacing 30, Font.justify ]
        |> paymentLayout "Paiement d'un atelier"


paymentSupportView : Model -> Statistics.Statistics -> Browser.Document Msg
paymentSupportView model statistics =
    let
        workflow =
            [ amountStep statistics "1", payStep "2" ]
    in
    [ [ "Je pratique un tarif libre. Vous pouvez me donner de l'argent pour soutenir mon activité professionelle" |> text ] |> paragraph []
    , Step.view (column [ spacing 50, width fill ]) workflow model model.step
    ]
        |> column [ spacing 30, Font.justify ]
        |> paymentLayout "Paiement d'un don"


success : Model -> Browser.Document Msg
success _ =
    [ "Merci pour votre paiement" |> text ]
        |> paragraph []
        |> paymentLayout "Merci"


cancel : Model -> Browser.Document Msg
cancel _ =
    [ "Paiement annulé" |> text ]
        |> paragraph []
        |> paymentLayout "Echec"


paymentLayout : String -> Element msg -> Browser.Document msg
paymentLayout title content =
    [ text title |> Typography.h1 []
    , content
    ]
        |> column [ spacing 30, width fill ]
        |> Layout.view (Just "payment") title

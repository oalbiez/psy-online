module Page.Report exposing (view)

import Browser
import Business.Statistics as Statistics
import Element exposing (column, fill, spacing, text, width)
import Element.Font as Font
import Page.Partial.Layout as Layout
import Page.Partial.ReportTable as ReportTable
import Style.Typography as Typography



---- VIEW ----


view : Statistics.Statistics -> Browser.Document msg
view statistics =
    [ "Bilans" |> text |> Typography.h1 []
    , column [ spacing 30, width fill ]
        [ ReportTable.monthlySummaryTable (ReportTable.Lines 6) statistics.payments statistics.work
        , "Derniers paiments" |> text |> Typography.h3 []
        , statistics.payments
            |> List.reverse
            |> ReportTable.paymentsTable (ReportTable.Lines 10)
        ]
    ]
        |> column [ spacing 30, width fill, Font.justify, Typography.scale 1 ]
        |> Layout.view (Just "report") "Finance"

module Page.About exposing (view)

import Browser
import Element exposing (column, fill, link, paragraph, spacing, text, width)
import Element.Font as Font
import Page.Partial.Layout as Layout
import Style.Theme as Theme
import Style.Typography as Typography



---- VIEW ----


view : Browser.Document msg
view =
    [ text "Qui suis-je ?" |> Typography.h1 []
    , paragraph []
        [ text "Je suis thérapeute, spécialisée en thérapie brève et en systémie, ainsi que coach en entreprise. Plus qu'un métier, le développement personnel, comme quête du mieux-être personnel, est pour moi une véritable passion, une philosophie de vie quotidienne. Je l'exerce en essayant de rester fidèle aux valeurs qui me sont chères : respect des différences, bienveillance, non-violence, goût pour le dépassement de soi."
        ]
    , text "Mon histoire" |> Typography.h1 []
    , paragraph []
        [ text "Aider les gens m'a toujours rendue heureuse. Le premier métier que j'ai voulu faire, en ce sens, a été ambulancière. Mais des ennuis de santé en ont décidé autrement..."
        ]
    , paragraph []
        [ text "Je me suis donc réorientée, pour devenir auxiliaire de vie sociale et aide-soignante. En exerçant cette activité comme indépendante, pour garder le choix de mes missions, j'ai pu me spécialiser en tant qu'accompagnatrice de personnes en fin de vie. Ce métier à été l'un des plus enrichissants de ma carrière, car j'ai pu y apporter le meilleur de moi-même."
        ]
    , paragraph []
        [ text "En parallèle, pour des raisons personnelles et sans lien avec mon métier, j'ai ressenti le besoin de suivre une thérapie. Outre le mieux-être et la paix d'esprit que cette démarche m'a apportées, découvrir les principes des thérapies brèves et de la systémie a été pour moi une véritable révélation. Rapidement, je me suis passionnée pour ces méthodes de développement personnel, séduite par leur efficacité et leur aspect ludique."
        ]
    , paragraph []
        [ text "À mon tour, j'ai eu envie de transmettre à d'autres personnes, ce cadeau qui m'avait été fait. C'est ainsi que j'ai suivi une formation à "
        , link [ Theme.link ]
            { url = "https://iicos.org/"
            , label = "l'institut iiCoS" |> text
            }
        , text ", pour devenir thérapeute et coach, et continuer à accompagner les gens d'une nouvelle manière. J'ai obtenu mon diplôme en janvier 2020. Depuis, j'exerce de manière très polyvalente, aussi bien en cabinet qu'en visioconférence ou en entreprise, pour pouvoir proposer des solutions sur-mesure à mes clients."
        ]
    ]
        |> column [ spacing 30, width fill, Font.justify ]
        |> Layout.view (Just "about") "À propos"

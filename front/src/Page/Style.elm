module Page.Style exposing (view)

import Browser
import Element exposing (Attribute, Element, column, el, fill, height, layout, padding, paddingXY, spacing, text, width)
import Element.Background as Background
import Element.Region as Region
import Style.Alert as Alert
import Style.Badge as Badge
import Style.Blockquote as Blockquote
import Style.Button as Button
import Style.Card as Card
import Style.Color as Color
import Style.Select as Select
import Style.Theme as Theme
import Style.Typography as Typography



-- VIEW


view : Browser.Document msg
view =
    { title = "Demo site - Styles"
    , body =
        [ layout [] <|
            column
                [ Region.mainContent
                , height fill
                , width fill
                , padding 50
                , spacing 30
                ]
                [ alert
                , badge
                , blockquote
                , button
                , card
                , select
                , typography
                ]
        ]
    }


alert : Element msg
alert =
    section "Alert" <|
        body <|
            [ Alert.alert [ width fill ] <| text "A simple primary alert."
            ]


badge : Element msg
badge =
    section "Badge" <|
        body <|
            [ Badge.badge [ Background.color Color.blue300 ] <| text "A badge"
            , Badge.pill [ Background.color Color.blue300 ] <| text "A pill"
            ]


blockquote : Element msg
blockquote =
    section "Blockquote" <|
        body <|
            [ text "A blockquote without source"
                |> Blockquote.blockquote Nothing
            , text "A blockquote with source"
                |> Blockquote.blockquote (Just (text "Nobody"))
            ]


button : Element msg
button =
    section "Button" <|
        body <|
            [ Button.button []
                { onPress = Nothing
                , label = "A simple button" |> text
                , color = Theme.primary
                }
            , Button.link []
                { url = "#"
                , label = "A link as button" |> text
                , color = Theme.primary
                }
            ]


card : Element msg
card =
    section "Card" <|
        body <|
            [ [ text "A simple card" |> Card.body [] ] |> Card.card []
            , [ text "Header" |> Card.header []
              , text "A card with header" |> Card.body []
              ]
                |> Card.card []
            , [ text "Header" |> Card.header []
              , text "A card" |> Card.body []
              , text "with multiples" |> Card.body []
              , text "bodies" |> Card.body []
              ]
                |> Card.card []
            , [ text "A card with footer" |> Card.body []
              , text "footer" |> Card.footer []
              ]
                |> Card.card []
            , [ text "Header" |> Card.header []
              , text "A card with header and footer" |> Card.body []
              , text "footer" |> Card.footer []
              ]
                |> Card.card []
            ]


select : Element msg
select =
    section "Select" <|
        body <|
            [ Select.select [ width (Element.px 200) ]
                { onSelect = Nothing
                , options =
                    [ Select.option "First" "0"
                    , Select.option "Second" "1"
                    ]
                }
            ]


typography : Element msg
typography =
    section "Typography - Heading " <|
        body <|
            [ Typography.h1 [] <| text "h1. Heading"
            , Typography.h2 [] <| text "h2. Heading"
            , Typography.h3 [] <| text "h3. Heading"
            , Typography.h4 [] <| text "h4. Heading"
            , Typography.h5 [] <| text "h5. Heading"
            , Typography.h6 [] <| text "h6. Heading"
            , Typography.textLead [] <| text "textLead"
            , Typography.textSmall [] <| text "textSmall"
            , Typography.textExtraSmall [] <| text "textExtraSmall"
            ]


makeSection : (List (Attribute msg) -> Element msg -> Element msg) -> String -> Element msg
makeSection level title =
    title
        |> text
        |> level [ width fill, Background.color Color.grey100, paddingXY 10 10 ]
        |> el [ width fill, paddingXY 0 10 ]


section : String -> Element msg -> Element msg
section title content =
    column [ width fill ]
        [ title |> makeSection Typography.h1
        , content
        ]



-- subsection : String -> Element msg -> Element msg
-- subsection title content =
--     column [ width fill ]
--         [ title |> makeSection Typography.h2
--         , content
--         ]


body : List (Element msg) -> Element msg
body content =
    content |> column [ width fill, paddingXY 50 0, spacing 20 ]

module Page.Home exposing (view)

import Browser
import Element exposing (alignBottom, column, el, fill, height, padding, paddingXY, paragraph, px, spacing, text, textColumn, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Page.Partial.Layout as Layout
import Style.Alert as Alert
import Style.Font as Font
import Style.Theme as Theme
import Style.Typography as Typography



---- VIEW ----


view : Browser.Document msg
view =
    [ text "Virginie Albiez"
        |> Typography.h1 [ alignBottom ]
        |> el
            [ height (px 150)
            , width fill
            , Background.image "images/fond.jpg"
            , Border.rounded 4
            , padding 10
            ]
    , textColumn [ spacing 20, width fill ]
        [ [ text "Je suis thérapeute, spécialisée en thérapie brève et en systémie, ainsi que coach en entreprise."
          ]
            |> paragraph []
        , [ text "Je travaille beaucoup sur les difficultés que rencontrent les personnes LGBT, particulièrement les personnes polyamoureuses et transgenres."
          ]
            |> paragraph []
        , [ text
                "J'accompagne aussi des réfugiés qui essaient de s'intégrer en France."
          ]
            |> paragraph []
        ]
    , [ text "Vous pouvez me contacter au 06 51 93 55 66" ]
        |> paragraph [ Typography.scale 3, Font.bold, Font.center ]
        |> Alert.alert
            [ paddingXY 30 10
            , Font.color Theme.primary.font
            , Background.color Theme.primary.background
            , Element.centerX
            ]

    -- , row [ spacing 20, paddingXY 10 20, Background.color Theme.navigation.background, width fill, Border.rounded 4 ]
    --     [ Button.link [ width fill, Font.center ]
    --         { url = "/#about"
    --         , label = [ Font.fa [] "\u{F007}", " Qui suis-je ?" |> text ] |> paragraph []
    --         , color = Theme.primary
    --         }
    --     , Button.link [ width fill, Font.center ]
    --         { url = "/#finance"
    --         , label = [ Font.fa [] "\u{F080}", " Rapport" |> text ] |> paragraph []
    --         , color = Theme.primary
    --         }
    --     , Button.link [ width fill, Font.center ]
    --         { url = "/#payment"
    --         , label = [ Font.fa [] "\u{F09D}", " Payer" |> text ] |> paragraph []
    --         , color = Theme.primary
    --         }
    --     ]
    ]
        |> column [ spacing 30, width fill, Font.justify ]
        |> Layout.view (Just "home") "Home"

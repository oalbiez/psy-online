module Page.NotFound exposing (view)

import Browser
import Element exposing (column, fill, paragraph, spacing, text, width)
import Page.Partial.Layout as Layout
import Style.Typography as Typography



-- VIEW


view : Browser.Document msg
view =
    [ text "Page non trouvée" |> Typography.h1 []
    , [ text ":/" ] |> paragraph []
    ]
        |> column [ spacing 30, width fill ]
        |> Layout.view Nothing "Page non trouvée"

module Page.Partial.Layout exposing (view)

import Browser
import Element exposing (Element)
import Style.Font as Font
import Style.Layout.LeftSideBar as Layout
import Style.Palette.Material as Color
import Style.Theme as Theme



---- VIEW ----


conf : Layout.Configuration msg
conf =
    { logo = "/logo.svg"
    , name = "Keilana"
    , menu =
        [ { id = "home", url = "/#", label = "Home", fa = "\u{F015}" }
        , { id = "about", url = "/#about", label = "À propos", fa = "\u{F007}" }
        , { id = "report", url = "/#finance", label = "Rapport", fa = "\u{F080}" }
        , { id = "payment", url = "/#payment", label = "Payer", fa = "\u{F09D}" }
        ]
    , navigationColor = Theme.navigation
    , contentColor =
        { background = Color.white
        , font = Color.black
        , hover = Color.blue300
        }
    , primaryColor = Theme.primary
    , navigationPortion = 1
    , contentPortion = 7
    , font = Font.withRoboto
    }


view : Maybe String -> String -> Element msg -> Browser.Document msg
view selected title content =
    Layout.view conf selected title content

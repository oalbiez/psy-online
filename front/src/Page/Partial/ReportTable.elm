module Page.Partial.ReportTable exposing
    ( Limit(..)
    , monthlyBalanceSheetTable
    , monthlySchedulesTable
    , monthlySummaryTable
    , paymentsTable
    , workingTimeTable
    )

import Business.Date as Date
import Business.Duration as Duration
import Business.Payment as Payment
import Business.PaymentStatistics as PaymentStatistics
import Business.Price as Price
import Business.Work as Work
import Business.WorkStatistics as WorkStatistics
import Dict
import Element exposing (Element, centerX, centerY, fill, table, text)
import List.Extra
import Style.Table as Table
import Time
import Tuple


type Limit
    = Lines Int


paymentsTable : Limit -> List Payment.Payment -> Element msg
paymentsTable (Lines limit) data =
    let
        reasonToString c =
            case c of
                Payment.Session duration ->
                    "Session " ++ Duration.toString duration

                Payment.Workshop ->
                    "Workshop"

                Payment.Support ->
                    "Don"
    in
    Table.table
        [ centerX
        , centerY
        ]
        { data = data |> List.take limit
        , columns =
            [ { header = "Date" |> text
              , width = fill
              , view = .when >> Date.toDMYString Time.utc >> text
              }
            , { header = "Montant" |> text
              , width = fill
              , view = .amount >> Price.toString >> text
              }
            , { header = "Catégorie" |> text
              , width = fill
              , view = .reason >> reasonToString >> text
              }
            ]
        }


monthlyBalanceSheetTable : Limit -> List Payment.Payment -> Element msg
monthlyBalanceSheetTable (Lines limit) data =
    let
        render f =
            Tuple.second >> f >> Price.toString >> text
    in
    Table.table
        [ centerX
        , centerY
        ]
        { data =
            data
                |> Payment.byMonth
                |> Dict.map (\_ -> PaymentStatistics.fromPayments)
                |> Dict.toList
                |> List.sortBy Tuple.first
                |> List.reverse
                |> List.take limit
        , columns =
            [ { header = "Mois" |> text
              , width = fill
              , view = Tuple.first >> Time.millisToPosix >> Date.toMYString Time.utc >> text
              }
            , { header = "Soutiens" |> text
              , width = fill
              , view = render .support
              }
            , { header = "Ateliers" |> text
              , width = fill
              , view = render .workshops
              }
            , { header = "Sessions" |> text
              , width = fill
              , view = render .sessions
              }
            , { header = "Total" |> text
              , width = fill
              , view = render PaymentStatistics.total
              }
            ]
        }


workingTimeTable : Limit -> List Work.Work -> Element msg
workingTimeTable (Lines limit) data =
    let
        categoryToString c =
            case c of
                Work.Individual ->
                    "Session"

                Work.Workshop ->
                    "Atelier"

                Work.Volunteer ->
                    "Bénévolat"
    in
    Table.table
        [ Element.centerX
        , Element.centerY
        ]
        { data = data |> List.take limit
        , columns =
            [ { header = "Date" |> Element.text
              , width = fill
              , view = .when >> Date.toDMYString Time.utc >> Element.text
              }
            , { header = "Durée" |> Element.text
              , width = fill
              , view = .duration >> Duration.toString >> Element.text
              }
            , { header = "Catégorie" |> Element.text
              , width = fill
              , view = .category >> categoryToString >> Element.text
              }
            ]
        }


monthlySchedulesTable : Limit -> List Work.Work -> Element msg
monthlySchedulesTable (Lines limit) data =
    let
        render f =
            Tuple.second >> f >> Duration.toString >> Element.text
    in
    Table.table
        [ Element.centerX
        , Element.centerY
        ]
        { data =
            data
                |> Work.byMonth
                |> Dict.map (\_ -> WorkStatistics.fromWorks)
                |> Dict.toList
                |> List.sortBy Tuple.first
                |> List.reverse
                |> List.take limit
        , columns =
            [ { header = "Mois" |> Element.text
              , width = fill
              , view =
                    Tuple.first >> Time.millisToPosix >> Date.toMYString Time.utc >> Element.text
              }
            , { header = "Bénévole" |> Element.text
              , width = fill
              , view = render .volonteer
              }
            , { header = "Individuel" |> Element.text
              , width = fill
              , view = render .individual
              }
            , { header = "Workshop" |> Element.text
              , width = fill
              , view = render .workshop
              }
            , { header = "Total" |> Element.text
              , width = fill
              , view = render WorkStatistics.total
              }
            ]
        }


monthlySummaryTable : Limit -> List Payment.Payment -> List Work.Work -> Element msg
monthlySummaryTable (Lines limit) payments works =
    let
        montlyPayment =
            payments
                |> Payment.balanceByMonth

        montlyWork =
            works
                |> Work.byMonth
                |> Dict.map (\_ -> WorkStatistics.fromWorks)

        months =
            Dict.keys montlyPayment
                ++ Dict.keys montlyWork
                |> List.sort
                |> List.Extra.unique
                |> List.map
                    (\m ->
                        ( m
                        , { work = Dict.get m montlyWork
                          , revenu = Dict.get m montlyPayment
                          }
                        )
                    )

        duration =
            Tuple.second >> .work >> Maybe.map WorkStatistics.total >> Maybe.map Duration.toString >> Maybe.withDefault "-" >> Element.text

        durationVolonteer =
            Tuple.second >> .work >> Maybe.map .volonteer >> Maybe.map Duration.toString >> Maybe.withDefault "-" >> Element.text

        price =
            Tuple.second >> .revenu >> Maybe.withDefault Price.zero >> Price.toString >> Element.text
    in
    Table.table
        [ centerX
        , centerY
        , Element.width fill
        ]
        { data =
            months
                |> List.sortBy Tuple.first
                |> List.reverse
                |> List.take limit
        , columns =
            [ { header = "Mois" |> text
              , width = fill
              , view = Tuple.first >> Time.millisToPosix >> Date.toMYString Time.utc >> text
              }
            , { header = "Temps de travail" |> text
              , width = fill
              , view = duration
              }
            , { header = "Bénévolat" |> text
              , width = fill
              , view = durationVolonteer
              }
            , { header = "Revenus" |> text
              , width = fill
              , view = price
              }
            ]
        }

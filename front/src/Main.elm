module Main exposing (main)

import Browser
import Browser.Events
import Browser.Navigation as Nav
import Business.Date as Date
import Business.Statistics exposing (Statistics, today, updateWorks)
import Business.Work as Work
import Cmd.Extra exposing (withCmd, withCmds, withNoCmd)
import Element
import Extra.Url
import Fixtures
import Http
import Json.Decode as D
import Maybe
import Page.About
import Page.Home
import Page.NotFound
import Page.Payment
import Page.Report
import Page.Style
import Platform.Sub
import Style.Browser
import Task
import Time
import Url
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, top)



---- MODEL ----


routeParser : Parser (Page -> a) a
routeParser =
    oneOf
        [ map HomePage top
        , map AboutPage (s "about")
        , map FinancePage (s "finance")
        , map PaymentPage (s "payment")
        , map SessionPaymentPage (s "payment" </> s "session")
        , map WorkshopPaymentPage (s "payment" </> s "workshop")
        , map SupportPaymentPage (s "payment" </> s "support")
        , map PaymentSuccess (s "payment" </> s "success")
        , map PaymentCancel (s "payment" </> s "cancel")
        , map StylePage (s "style")
        ]


type Page
    = HomePage
    | AboutPage
    | FinancePage
    | PaymentPage
    | SessionPaymentPage
    | WorkshopPaymentPage
    | SupportPaymentPage
    | PaymentSuccess
    | PaymentCancel
    | NotFound
    | StylePage


type alias Model =
    { key : Nav.Key
    , device : Element.Device
    , page : Page
    , statistics : Statistics
    , paymentSession : Page.Payment.Model
    }


init : { width : Int, height : Int } -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init window url key =
    { key = key
    , device = Element.classifyDevice window
    , page = toRoute url
    , statistics =
        { today = Date.date 2020 Time.Jun 15 |> Maybe.withDefault Date.origin
        , work = Fixtures.works
        , payments = Fixtures.payments
        }
    , paymentSession = Page.Payment.init url key
    }
        |> withCmds
            [ Task.perform OnTime Time.now
            , Http.get
                { url = "http://localhost:8070/sessions"
                , expect = Http.expectJson GotWork <| D.list Work.decoder
                }
            ]



---- UPDATE ----


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | WindowResized Int Int
    | PaymentSessionMsg Page.Payment.Msg
    | GotWork (Result Http.Error (List Work.Work))
    | OnTime Time.Posix


toRoute : Url.Url -> Page
toRoute url =
    url
        |> Extra.Url.withFragment
        |> parse routeParser
        |> Maybe.withDefault NotFound


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked (Browser.Internal url) ->
            model
                |> withCmd (Nav.pushUrl model.key (Url.toString url))

        LinkClicked (Browser.External url) ->
            model
                |> withCmd (Nav.load url)

        UrlChanged url ->
            -- update from url
            { model | page = toRoute url }
                |> withNoCmd

        WindowResized width height ->
            { model | device = Element.classifyDevice { width = width, height = height } }
                |> withNoCmd

        PaymentSessionMsg submessage ->
            let
                r =
                    Page.Payment.update submessage model.paymentSession
            in
            { model | paymentSession = r |> Tuple.first }
                |> withCmd (r |> Tuple.second |> Cmd.map PaymentSessionMsg)

        GotWork result ->
            (case result of
                Ok works ->
                    { model | statistics = model.statistics |> updateWorks works }

                Err _ ->
                    model
            )
                |> withNoCmd

        OnTime time ->
            { model | statistics = model.statistics |> today time }
                |> withNoCmd



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Platform.Sub.batch
        [ Browser.Events.onResize WindowResized
        ]



---- VIEW ----


view : Model -> Browser.Document Msg
view model =
    case model.page of
        HomePage ->
            Page.Home.view

        AboutPage ->
            Page.About.view

        FinancePage ->
            Page.Report.view model.statistics

        PaymentPage ->
            Page.Payment.paymentView
                |> Style.Browser.map PaymentSessionMsg

        SessionPaymentPage ->
            Page.Payment.paymentSessionView model.paymentSession model.statistics
                |> Style.Browser.map PaymentSessionMsg

        WorkshopPaymentPage ->
            Page.Payment.paymentWorkshopView model.paymentSession model.statistics
                |> Style.Browser.map PaymentSessionMsg

        SupportPaymentPage ->
            Page.Payment.paymentSupportView model.paymentSession model.statistics
                |> Style.Browser.map PaymentSessionMsg

        PaymentSuccess ->
            Page.Payment.success model.paymentSession
                |> Style.Browser.map PaymentSessionMsg

        PaymentCancel ->
            Page.Payment.cancel model.paymentSession
                |> Style.Browser.map PaymentSessionMsg

        StylePage ->
            Page.Style.view

        NotFound ->
            Page.NotFound.view



--|> Style.Browser.map SessionSub
---- PROGRAM ----


main : Program { width : Int, height : Int } Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }

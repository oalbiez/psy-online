module Fixtures exposing (payments, works)

import Business.Date as Date
import Business.Duration as Duration
import Business.Payment as Payment
import Business.Price as Price
import Business.Work as Work
import Maybe
import Time


date : Int -> Time.Month -> Int -> Date.Date
date year month day =
    Date.date year month day
        |> Maybe.withDefault Date.origin


payments : List Payment.Payment
payments =
    [ { when = date 2020 Time.Feb 12
      , amount = Price.fromFloat 60.0
      , reason = Payment.Workshop
      }
    , { when = date 2020 Time.May 6
      , amount = Price.fromFloat 60.0
      , reason = Payment.Session (Duration.minutes 120)
      }
    , { when = date 2020 Time.May 25
      , amount = Price.fromFloat 40.0
      , reason = Payment.Session (Duration.minutes 60)
      }
    , { when = date 2020 Time.Jun 10
      , amount = Price.fromFloat 100.0
      , reason = Payment.Session (Duration.minutes 120)
      }
    , { when = date 2020 Time.Jun 12
      , amount = Price.fromFloat 20.0
      , reason = Payment.Session (Duration.minutes 60)
      }
    , { when = date 2020 Time.Jun 12
      , amount = Price.fromFloat 70.0
      , reason = Payment.Session (Duration.minutes 60)
      }
    , { when = date 2020 Time.Jun 15
      , amount = Price.fromFloat 65.0
      , reason = Payment.Session (Duration.minutes 120)
      }
    , { when = date 2020 Time.Jul 3
      , amount = Price.fromFloat 70.0
      , reason = Payment.Session (Duration.minutes 60)
      }
    , { when = date 2020 Time.Jul 3
      , amount = Price.fromFloat 70.0
      , reason = Payment.Session (Duration.minutes 90)
      }
    , { when = date 2020 Time.Jul 13
      , amount = Price.fromFloat 65.0
      , reason = Payment.Session (Duration.minutes 120)
      }
    ]


works : List Work.Work
works =
    [ { when = date 2020 Time.Feb 12
      , duration = Duration.minutes 180
      , category = Work.Workshop
      }
    , { when = date 2020 Time.Feb 14
      , duration = Duration.minutes 240
      , category = Work.Volunteer
      }
    , { when = date 2020 Time.Mar 4
      , duration = Duration.minutes 180
      , category = Work.Volunteer
      }
    , { when = date 2020 Time.Apr 14
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Apr 15
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Apr 28
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Apr 29
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 1
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 7
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 8
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 12
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 20
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 25
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 26
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 27
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.May 29
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 6
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 10
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 12
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 13
      , duration = Duration.minutes 30
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 17
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 21
      , duration = Duration.minutes 30
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 21
      , duration = Duration.minutes 30
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 23
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jun 30
      , duration = Duration.minutes 60
      , category = Work.Individual
      }

    --
    , { when = date 2020 Time.Jul 1
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jul 2
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jul 8
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jul 9
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    , { when = date 2020 Time.Jul 14
      , duration = Duration.minutes 60
      , category = Work.Individual
      }
    ]

module Extra.Url exposing (useFragment, withFragment)

import Url
import Url.Builder exposing (absolute)


fragment : Url.Url -> String
fragment =
    .fragment >> Maybe.withDefault ""


useFragment : Url.Url -> Url.Url
useFragment url =
    { url | path = absolute [ url |> fragment ] [], fragment = Nothing }


withFragment : Url.Url -> Url.Url
withFragment url =
    { url | path = url.path ++ (url |> fragment), fragment = Nothing }

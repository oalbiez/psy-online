module Style.Table exposing
    ( table
    , tableCell
    , tableHeader
    )

import Element
    exposing
        ( Attribute
        , Column
        , Element
        , el
        , paddingXY
        )
import Element.Border as Border
import Element.Font as Font
import Style.Color


tableHeader : Element msg -> Element msg
tableHeader item =
    item |> el [ Font.bold, paddingXY 5 10, Border.widthXY 0 1, Border.color Style.Color.grey200 ]


tableCell : Element msg -> Element msg
tableCell item =
    item
        |> el
            [ paddingXY 5 2
            , Border.color Style.Color.grey200
            , Border.widthEach
                { bottom = 0
                , left = 0
                , right = 0
                , top = 1
                }
            ]


table :
    List (Attribute msg)
    ->
        { data : List records
        , columns : List (Column records msg)
        }
    -> Element msg
table attributes { data, columns } =
    let
        update column =
            { column
                | header = column.header |> tableHeader
                , view = tableCell << column.view
            }
    in
    Element.table
        attributes
        { data = data
        , columns = columns |> List.map update
        }

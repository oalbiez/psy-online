module Style.Step exposing (Aggregate, Step, stepView, view)

import Element exposing (Element, alignTop, el, fill, height, padding, row, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Style.Theme as Theme
import Style.Typography as Typography


type alias Step model msg =
    { past : model -> Element msg
    , current : model -> Element msg
    , futur : model -> Element msg
    }


type alias Aggregate msg =
    List (Element msg) -> Element msg


stepView : Theme.ColorModel -> String -> Element msg -> Element msg
stepView color position content =
    [ position
        |> text
        |> el
            [ alignTop
            , padding 10
            , height fill
            , Typography.scale 4
            , Font.bold
            , Font.color color.font
            , Background.color color.background
            , Border.rounded 4
            ]
    , content |> el [ padding 10, width fill ]
    ]
        |> row [ spacing 20, width fill ]


view : Aggregate msg -> List (Step model msg) -> model -> Int -> Element msg
view aggregator steps model position =
    let
        render index step =
            if index < position then
                step.past model

            else if index == position then
                step.current model

            else
                step.futur model
    in
    steps |> List.indexedMap render |> aggregator

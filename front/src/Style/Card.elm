module Style.Card exposing (body, card, footer, header)

import Element exposing (Attribute, Color, Element, column, el, fill, rgba, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Style.Color as Color


borderColor : Color
borderColor =
    rgba 0.0 0.0 0.0 0.3


header : List (Attribute msg) -> Element msg -> Element msg
header attribute child =
    let
        style =
            Element.padding 10
                :: Font.bold
                :: Background.color Color.grey200
                :: width fill
                :: Border.roundEach
                    { topLeft = 4
                    , topRight = 4
                    , bottomLeft = 0
                    , bottomRight = 0
                    }
                :: attribute
                |> el
    in
    child |> style


footer : List (Attribute msg) -> Element msg -> Element msg
footer attribute child =
    let
        style =
            Element.padding 10
                :: Background.color Color.grey200
                :: width fill
                :: Border.roundEach
                    { topLeft = 0
                    , topRight = 0
                    , bottomLeft = 4
                    , bottomRight = 4
                    }
                :: attribute
                |> el
    in
    child |> style


body : List (Attribute msg) -> Element msg -> Element msg
body attribute child =
    let
        style =
            Element.padding 20
                :: width fill
                :: attribute
                |> el
    in
    child |> style


card : List (Attribute msg) -> List (Element msg) -> Element msg
card attributes children =
    let
        shadow =
            { blur = 10, color = rgba 0 0 0 0.05, offset = ( 0, 2 ), size = 1 }

        border =
            Border.width 1
                :: Border.solid
                :: Border.rounded 4
                :: Border.color borderColor
                :: Border.shadow shadow
                :: width fill
                :: attributes
                |> column

        item i =
            if i + 1 == List.length children then
                el [ width fill ]

            else
                el
                    [ Border.color borderColor
                    , Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }
                    , width fill
                    ]
    in
    children
        |> List.indexedMap item
        |> border

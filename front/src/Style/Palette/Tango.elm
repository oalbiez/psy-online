module Style.Palette.Tango exposing
    ( red, orange, yellow, green, blue, purple, brown
    , lightOrange, lightYellow, lightGreen, lightBlue, lightPurple, lightBrown
    , darkRed, darkOrange, darkYellow, darkGreen, darkBlue, darkPurple, darkBrown
    , white, lightGrey, grey, darkGrey, lightCharcoal, charcoal, darkCharcoal, black
    , aluminium1, aluminium2, aluminium3, aluminium4, aluminium5, aluminium6, butter, chameleon, chocolate, darkButter, darkChameleon, darkChocolate, darkPlum, darkScarletRed, darkSkyBlue, lighRed, lightButter, lightChameleon, lightChocolate, lightPlum, lightScarletRed, lightSkyBlue, plum, scarletRed, skyBlue
    )

{-| These colors come from the [Tango palette](http://tango.freedesktop.org/Tango_Icon_Theme_Guidelines)
which provides aesthetically reasonable defaults for colors.
Each color also comes with a light and dark version.


## Standard

@docs red, orange, yellow, green, blue, purple, brown


## Light

@docs lightRed, lightOrange, lightYellow, lightGreen, lightBlue, lightPurple, lightBrown


## Dark

@docs darkRed, darkOrange, darkYellow, darkGreen, darkBlue, darkPurple, darkBrown


## Eight Shades of Grey

These colors are a compatible series of shades of grey, fitting nicely
with the Tango palette.

@docs white, lightGrey, grey, darkGrey, lightCharcoal, charcoal, darkCharcoal, black

-}

import Element exposing (Color, rgb255)


{-| -}
black : Color
black =
    rgb255 0 0 0


{-| -}
white : Color
white =
    rgb255 255 255 255



-- Butter


{-| -}
lightButter : Color
lightButter =
    rgb255 255 233 79


{-| -}
butter : Color
butter =
    rgb255 237 212 0


{-| -}
darkButter : Color
darkButter =
    rgb255 196 160 0


{-| -}
lightYellow : Color
lightYellow =
    lightButter


{-| -}
yellow : Color
yellow =
    butter


{-| -}
darkYellow : Color
darkYellow =
    darkButter



-- Orange


{-| -}
lightOrange : Color
lightOrange =
    rgb255 252 175 62


{-| -}
orange : Color
orange =
    rgb255 245 121 0


{-| -}
darkOrange : Color
darkOrange =
    rgb255 206 92 0



-- Chocolate


{-| -}
lightChocolate : Color
lightChocolate =
    rgb255 233 185 110


{-| -}
chocolate : Color
chocolate =
    rgb255 193 125 17


{-| -}
darkChocolate : Color
darkChocolate =
    rgb255 143 89 2


{-| -}
lightBrown : Color
lightBrown =
    lightChocolate


{-| -}
brown : Color
brown =
    chocolate


{-| -}
darkBrown : Color
darkBrown =
    darkChocolate



-- Chameleon


{-| -}
lightChameleon : Color
lightChameleon =
    rgb255 138 226 52


{-| -}
chameleon : Color
chameleon =
    rgb255 115 210 22


{-| -}
darkChameleon : Color
darkChameleon =
    rgb255 78 154 6


{-| -}
lightGreen : Color
lightGreen =
    lightChameleon


{-| -}
green : Color
green =
    chameleon


{-| -}
darkGreen : Color
darkGreen =
    darkChameleon



-- Sky Blue


{-| -}
lightSkyBlue : Color
lightSkyBlue =
    rgb255 114 159 207


{-| -}
skyBlue : Color
skyBlue =
    rgb255 52 101 164


{-| -}
darkSkyBlue : Color
darkSkyBlue =
    rgb255 32 74 135


{-| -}
lightBlue : Color
lightBlue =
    lightSkyBlue


{-| -}
blue : Color
blue =
    skyBlue


{-| -}
darkBlue : Color
darkBlue =
    darkSkyBlue



-- Plum


{-| -}
lightPlum : Color
lightPlum =
    rgb255 173 127 168


{-| -}
plum : Color
plum =
    rgb255 117 80 123


{-| -}
darkPlum : Color
darkPlum =
    rgb255 92 53 102


{-| -}
lightPurple : Color
lightPurple =
    rgb255 173 127 168


{-| -}
purple : Color
purple =
    rgb255 117 80 123


{-| -}
darkPurple : Color
darkPurple =
    rgb255 92 53 102



-- Scarlet Red


{-| -}
lightScarletRed : Color
lightScarletRed =
    rgb255 239 41 41


{-| -}
scarletRed : Color
scarletRed =
    rgb255 204 0 0


{-| -}
darkScarletRed : Color
darkScarletRed =
    rgb255 164 0 0


lighRed : Color
lighRed =
    lightScarletRed


red : Color
red =
    scarletRed


darkRed : Color
darkRed =
    darkScarletRed



-- Aluminium


{-| -}
aluminium1 : Color
aluminium1 =
    rgb255 238 238 236


{-| -}
aluminium2 : Color
aluminium2 =
    rgb255 211 215 207


{-| -}
aluminium3 : Color
aluminium3 =
    rgb255 186 189 182


{-| -}
aluminium4 : Color
aluminium4 =
    rgb255 136 138 133


{-| -}
aluminium5 : Color
aluminium5 =
    rgb255 85 87 83


{-| -}
aluminium6 : Color
aluminium6 =
    rgb255 46 52 54


{-| -}
lightGrey : Color
lightGrey =
    aluminium1


{-| -}
grey : Color
grey =
    aluminium2


{-| -}
darkGrey : Color
darkGrey =
    aluminium3


{-| -}
lightCharcoal : Color
lightCharcoal =
    aluminium4


{-| -}
charcoal : Color
charcoal =
    aluminium5


{-| -}
darkCharcoal : Color
darkCharcoal =
    aluminium6

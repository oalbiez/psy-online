module Style.Theme exposing
    ( ColorModel
    , danger
    , info
    , link
    , navigation
    , primary
    , secondary
    , success
    , warning
    )

import Element exposing (Attribute, Color)
import Element.Font as Font
import Style.Color


type alias ColorModel =
    { background : Color
    , font : Color
    , hover : Color
    }


primary : ColorModel
primary =
    { background = Style.Color.lightBlueA400
    , font = Style.Color.white
    , hover = Style.Color.lightBlueA700
    }


secondary : ColorModel
secondary =
    { background = Style.Color.grey500
    , font = Style.Color.white
    , hover = Style.Color.grey700
    }


success : ColorModel
success =
    { background = Style.Color.greenA400
    , font = Style.Color.white
    , hover = Style.Color.greenA700
    }


danger : ColorModel
danger =
    { background = Style.Color.redA400
    , font = Style.Color.white
    , hover = Style.Color.redA700
    }


warning : ColorModel
warning =
    { background = Style.Color.yellowA400
    , font = Style.Color.black
    , hover = Style.Color.yellowA700
    }


info : ColorModel
info =
    { background = Style.Color.cyanA400
    , font = Style.Color.black
    , hover = Style.Color.cyanA700
    }


navigation : ColorModel
navigation =
    { background = Style.Color.grey700
    , font = Style.Color.white
    , hover = Style.Color.grey700
    }


link : Attribute msg
link =
    Font.color Style.Color.blue700

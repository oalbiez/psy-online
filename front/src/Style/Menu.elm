module Style.Menu exposing (Entry)


type alias Entry =
    { id : String
    , url : String
    , label : String
    , fa : String
    }

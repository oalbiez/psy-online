module Style.Layout.LeftSideBar exposing (Configuration, view)

import Browser
import Element exposing (Attribute, Element, centerX, centerY, column, fill, fillPortion, height, image, layout, link, padding, paddingEach, paragraph, px, row, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font
import Element.Region as Region
import Style.Font as Font
import Style.Menu as Menu
import Style.Theme as Theme
import Style.Typography as Typography


renderMenu : List Menu.Entry -> Maybe String -> Theme.ColorModel -> List (Element msg)
renderMenu menu selected highlightColor =
    let
        default =
            [ padding 5, width fill, Border.rounded 4 ]

        highlight =
            Background.color highlightColor.background
                :: Element.Font.color highlightColor.font
                :: default

        match id =
            selected |> Maybe.map ((==) id) |> Maybe.withDefault False

        entry e =
            link
                (if match e.id then
                    highlight

                 else
                    default
                )
                { url = e.url
                , label =
                    row [ spacing 10 ]
                        [ Font.fa [ width (Element.px 20), Element.Font.center ] e.fa
                        , text e.label
                        ]
                }
    in
    menu |> List.map entry


type alias Configuration msg =
    { logo : String
    , name : String
    , menu : List Menu.Entry
    , navigationColor : Theme.ColorModel
    , contentColor : Theme.ColorModel
    , primaryColor : Theme.ColorModel
    , navigationPortion : Int
    , contentPortion : Int
    , font : Attribute msg
    }


view : Configuration msg -> Maybe String -> String -> Element msg -> Browser.Document msg
view conf selected title content =
    { title = conf.name ++ " - " ++ title
    , body =
        [ layout [] <|
            row [ height fill, width fill, conf.font ]
                [ navigationView conf selected
                , contentView conf content
                ]
        ]
    }


logoView : Configuration msg -> Element msg
logoView conf =
    let
        size =
            Typography.scaled 2
    in
    [ image [ height (px size), centerX, centerY ]
        { src = conf.logo
        , description = "logo"
        }
    , conf.name |> text |> List.singleton |> paragraph [ Element.Font.size size, centerY ]
    ]
        |> row
            [ width fill
            , spacing 10
            , paddingEach
                { top = 10
                , right = 0
                , bottom = 50
                , left = 0
                }
            ]


navigationView : Configuration msg -> Maybe String -> Element msg
navigationView conf selected =
    logoView conf
        :: renderMenu conf.menu selected conf.primaryColor
        |> column
            [ Region.navigation
            , height fill
            , conf.navigationPortion |> fillPortion |> width
            , padding 15
            , spacing 5
            , conf.navigationColor.background |> Background.color
            , conf.navigationColor.font |> Element.Font.color
            ]


contentView : Configuration msg -> Element msg -> Element msg
contentView conf content =
    content
        |> List.singleton
        |> column
            [ Region.mainContent
            , height fill
            , conf.contentPortion |> fillPortion |> width
            , padding 50
            , conf.contentColor.background |> Background.color
            , conf.contentColor.font |> Element.Font.color
            ]

module Style.Badge exposing (badge, pill)

import Element exposing (Attribute, Element, el, paddingEach)
import Element.Border as Border
import Element.Font as Font


badge : List (Attribute msg) -> Element msg -> Element msg
badge attributes child =
    let
        box =
            paddingEach { top = 3, right = 5, bottom = 3, left = 5 }
                :: Border.rounded 4
                :: Font.size 12
                :: Font.center
                :: Font.bold
                :: attributes
                |> el
    in
    child |> box


pill : List (Attribute msg) -> Element msg -> Element msg
pill attributes child =
    let
        box =
            paddingEach { top = 3, right = 5, bottom = 3, left = 5 }
                :: Border.rounded 160
                :: Font.size 12
                :: Font.center
                :: Font.bold
                :: attributes
                |> el
    in
    child |> box

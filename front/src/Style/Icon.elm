module Style.Icon exposing (creditCard, home)

import Element
import Svg
import Svg.Attributes as SA


intToHex : Int -> String
intToHex value =
    let
        radix =
            16

        digit i =
            if i < 10 then
                String.fromChar <| Char.fromCode <| i + Char.toCode '0'

            else
                String.fromChar <| Char.fromCode <| i - 10 + Char.toCode 'A'
    in
    if value < radix then
        digit value

    else
        intToHex (value // radix) ++ digit (modBy radix value)


colorToHex : Element.Color -> String
colorToHex color =
    let
        c =
            Element.toRgb color

        toHex v =
            v * 255 |> round |> intToHex
    in
    "#" ++ ([ c.red, c.green, c.blue, c.alpha ] |> List.map toHex |> String.join "")


toPx : Int -> String
toPx =
    String.fromInt


home : Element.Color -> Int -> Element.Element msg
home color size =
    Element.html <|
        Svg.svg [ size |> toPx |> SA.height, SA.viewBox "0 0 1536 1536" ]
            [ Svg.path [ SA.d "M1408 992v480c0 35-29 64-64 64H960v-384H704v384H320c-35 0-64-29-64-64V992c0-2 1-4 1-6l575-474 575 474c1 2 1 4 1 6zm223-69l-62 74c-5 6-13 10-21 11h-3c-8 0-15-2-21-7L832 424l-692 577c-7 5-15 8-24 7-8-1-16-5-21-11l-62-74c-11-13-9-34 4-45l719-599c42-35 110-35 152 0l244 204V288c0-18 14-32 32-32h192c18 0 32 14 32 32v408l219 182c13 11 15 32 4 45z", color |> colorToHex |> SA.fill ] []
            ]


creditCard : Element.Color -> Int -> Element.Element msg
creditCard color size =
    Element.html <|
        Svg.svg [ size |> toPx |> SA.height, SA.viewBox "0 0 1920 1920" ]
            [ Svg.path [ SA.d "M1760 128c88 0 160 72 160 160v1216c0 88-72 160-160 160H160c-88 0-160-72-160-160V288c0-88 72-160 160-160h1600zM160 256c-17 0-32 15-32 32v224h1664V288c0-17-15-32-32-32H160zm1600 1280c17 0 32-15 32-32V896H128v608c0 17 15 32 32 32h1600zM256 1408v-128h256v128H256zm384 0v-128h384v128H640z", color |> colorToHex |> SA.fill ] []
            ]

module Style.Select exposing (option, select)

import Element exposing (Attribute, Element, el, paddingEach)
import Html
import Html.Attributes exposing (value)
import Html.Events exposing (onInput)


type alias Option =
    { label : String
    , value : String
    }


option : String -> String -> Option
option =
    Option


select :
    List (Attribute msg)
    ->
        { onSelect : Maybe (String -> msg)
        , options : List Option
        }
    -> Element msg
select attributes { onSelect, options } =
    let
        style =
            paddingEach { top = 6, right = 12, bottom = 6, left = 12 }
                :: attributes

        action =
            case onSelect of
                Just f ->
                    [ onInput f ]

                _ ->
                    []

        optionize item =
            Html.option [ value item.value ] [ item.label |> Html.text ]
    in
    options
        |> List.map optionize
        |> Html.select action
        |> Element.html
        |> el style

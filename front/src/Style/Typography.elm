module Style.Typography exposing
    ( h1
    , h2
    , h3
    , h4
    , h5
    , h6
    , scale
    , scaled
    , textExtraSmall
    , textLead
    , textSmall
    )

import Element exposing (Attribute, Element, alignLeft, el, paddingEach)
import Element.Font as Font
import Element.Region as Region


scaled : Int -> Int
scaled =
    Element.modular 16 1.25 >> round


scale : Int -> Attribute msg
scale =
    scaled >> Font.size


h1 : List (Attribute msg) -> Element msg -> Element msg
h1 =
    heading 1 >> el


{-| -}
h2 : List (Attribute msg) -> Element msg -> Element msg
h2 =
    heading 2 >> el


{-| -}
h3 : List (Attribute msg) -> Element msg -> Element msg
h3 =
    heading 3 >> el


{-| -}
h4 : List (Attribute msg) -> Element msg -> Element msg
h4 =
    heading 4 >> el


{-| -}
h5 : List (Attribute msg) -> Element msg -> Element msg
h5 =
    heading 5 >> el


{-| -}
h6 : List (Attribute msg) -> Element msg -> Element msg
h6 =
    heading 6 >> el


{-| -}
textLead : List (Attribute msg) -> Element msg -> Element msg
textLead =
    section 3 >> el


{-| -}
textSmall : List (Attribute msg) -> Element msg -> Element msg
textSmall =
    section -1 >> el


{-| -}
textExtraSmall : List (Attribute msg) -> Element msg -> Element msg
textExtraSmall =
    section -2 >> el


heading : Int -> List (Attribute msg) -> List (Attribute msg)
heading level attributes =
    let
        size =
            7 - level |> clamp 1 6
    in
    Region.heading level
        :: paddingEach { top = 16, right = 0, bottom = 8, left = 0 }
        :: alignLeft
        :: Font.bold
        :: scale size
        :: attributes


section : Int -> List (Attribute msg) -> List (Attribute msg)
section size attributes =
    scale size :: attributes

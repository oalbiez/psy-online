module Style.Browser exposing (map)

import Browser
import Html


map : (a -> b) -> Browser.Document a -> Browser.Document b
map to { title, body } =
    { title = title, body = body |> List.map (Html.map to) }

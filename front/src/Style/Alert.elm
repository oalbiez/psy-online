module Style.Alert exposing (alert)

import Element exposing (Attribute, Element, el, paddingEach, width)
import Element.Border as Border


alert : List (Attribute msg) -> Element msg -> Element msg
alert attributes child =
    let
        box =
            paddingEach { top = 20, right = 12, bottom = 20, left = 12 }
                :: Border.width 1
                :: Border.solid
                :: Border.rounded 4
                :: Border.color (Element.rgba 0.0 0.0 0.0 0.3)
                :: attributes
                |> el
    in
    child |> box

module Style.Button exposing (button, link)

import Element
    exposing
        ( Attribute
        , Element
        , mouseOver
        , paddingEach
        , rgba
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Style.Theme as Theme


style : List (Attribute msg) -> Theme.ColorModel -> List (Attribute msg)
style attributes color =
    paddingEach { top = 6, right = 12, bottom = 6, left = 12 }
        :: Border.width 1
        :: Border.solid
        :: Border.rounded 4
        :: Border.color (Element.rgba 0.0 0.0 0.0 0.3)
        :: Background.color color.background
        :: mouseOver [ Background.color color.hover ]
        :: Font.color color.font
        :: attributes


button :
    List (Attribute msg)
    ->
        { onPress : Maybe msg
        , label : Element msg
        , color : Theme.ColorModel
        }
    -> Element msg
button attributes { onPress, label, color } =
    Input.button (style attributes color)
        { onPress = onPress
        , label = label
        }


link :
    List (Attribute msg)
    ->
        { url : String
        , label : Element msg
        , color : Theme.ColorModel
        }
    -> Element msg
link attributes { url, label, color } =
    Element.link (style attributes color)
        { url = url
        , label = label
        }

module Style.Font exposing (fa, forkAwesome, roboto, withForkAwesome, withRoboto)

import Element exposing (Attribute)
import Element.Font as Font


roboto : Font.Font
roboto =
    Font.external
        { name = "Roboto"
        , url = "https://fonts.googleapis.com/css?family=Roboto"
        }


withRoboto : Attribute msg
withRoboto =
    Font.family [ roboto, Font.sansSerif ]


forkAwesome : Font.Font
forkAwesome =
    Font.external
        { name = "ForkAwesome"
        , url = "https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css"
        }


withForkAwesome : Attribute msg
withForkAwesome =
    Font.family [ forkAwesome ]


fa : List (Attribute msg) -> String -> Element.Element msg
fa attributes =
    let
        style =
            withForkAwesome :: attributes
    in
    Element.text >> Element.el style

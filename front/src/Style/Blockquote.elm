module Style.Blockquote exposing (blockquote)

import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , paddingEach
        , paragraph
        , text
        , width
        )
import Element.Font as Font
import Style.Color as Color


blockquote : Maybe (Element msg) -> Element msg -> Element msg
blockquote source content =
    let
        addSource s =
            paragraph
                [ Font.color Color.grey400
                , paddingEach { top = 10, right = 0, bottom = 0, left = 0 }
                , width fill
                , Font.alignRight
                ]
                [ text "—\u{00A0}", s ]
    in
    column
        [ paddingEach { top = 5, right = 30, bottom = 5, left = 30 }, width fill ]
        [ content |> el [ Font.size 24 ]
        , source |> Maybe.map addSource |> Maybe.withDefault Element.none
        ]

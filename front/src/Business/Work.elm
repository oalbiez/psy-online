module Business.Work exposing (Category(..), Work, byMonth, decoder, encode, totalByMonth)

import Business.Date as Date exposing (encode)
import Business.Duration as Duration
import Dict
import Dict.Extra as DictExtra
import Json.Decode as D
import Json.Encode as E
import Json.Helpers


type Category
    = Individual
    | Workshop
    | Volunteer


type alias Work =
    { when : Date.Date
    , duration : Duration.Duration
    , category : Category
    }



-- ENCODE


encode : Work -> E.Value
encode work =
    E.object
        [ ( "when", Date.encode work.when )
        , ( "duration", Duration.encode work.duration )
        , ( "category", encodeCategory work.category )
        ]


encodeCategory : Category -> E.Value
encodeCategory category =
    E.string <|
        case category of
            Individual ->
                "individual"

            Workshop ->
                "workshop"

            Volunteer ->
                "volunteer"



-- DECODER


decoder : D.Decoder Work
decoder =
    D.map3 Work
        (D.field "when" Date.decoder)
        (D.field "duration" Duration.decoder)
        (D.field "category" categoryDecoder)


categoryDecoder : D.Decoder Category
categoryDecoder =
    Json.Helpers.decodeSumUnaries "Category" <|
        Dict.fromList
            [ ( "individual", Individual )
            , ( "workshop", Workshop )
            , ( "volunteer", Volunteer )
            ]


sum : List Work -> Duration.Duration
sum =
    List.map .duration >> Duration.sum


byMonth : List Work -> Dict.Dict Date.MY (List Work)
byMonth =
    DictExtra.groupBy (\x -> x.when |> Date.toMY)


totalByMonth : List Work -> Dict.Dict Date.MY Duration.Duration
totalByMonth =
    byMonth >> Dict.map (\_ -> sum)

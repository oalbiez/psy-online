module Business.Duration exposing (Duration(..), add, decoder, encode, hours, minutes, sum, toHours, toMinutes, toString, zero)

import Json.Decode as D
import Json.Encode as E
import TypedTime


type Duration
    = Duration Int


encode : Duration -> E.Value
encode (Duration value) =
    E.int value


decoder : D.Decoder Duration
decoder =
    D.int |> D.map minutes


zero : Duration
zero =
    minutes 0


minutes : Int -> Duration
minutes =
    Duration


hours : Float -> Duration
hours =
    (*) 60 >> round >> Duration


add : Duration -> Duration -> Duration
add (Duration x) (Duration y) =
    Duration (x + y)


toHours : Duration -> Float
toHours (Duration value) =
    toFloat value / 60.0


toMinutes : Duration -> Int
toMinutes (Duration value) =
    value


toString : Duration -> String
toString (Duration value) =
    value |> toFloat |> TypedTime.minutes |> TypedTime.toString TypedTime.Hours


sum : List Duration -> Duration
sum =
    List.foldr add zero

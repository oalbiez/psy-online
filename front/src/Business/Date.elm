module Business.Date exposing (Date, MY, date, decoder, encode, happendOn, origin, sameMonth, toDMYString, toMY, toMYString)

import Calendar
import Clock
import DateTime
import Derberos.Date.Calendar as DerberosCalendar
import Flip exposing (flip)
import Json.Decode as D
import Json.Encode as E
import Maybe exposing (Maybe)
import Time
import Time.Format exposing (format)
import Time.Format.Config.Config_fr_fr exposing (config)


type alias Date =
    Time.Posix


type alias MY =
    Int


encode : Date -> E.Value
encode value =
    E.int <| Time.posixToMillis value


decoder : D.Decoder Date
decoder =
    D.string
        |> D.andThen
            (\value ->
                case String.split "-" value of
                    [ y_, m_, d_ ] ->
                        Maybe.map3
                            (\a b c -> D.succeed (date a b c |> Maybe.withDefault origin))
                            (String.right 4 y_ |> String.toInt)
                            (String.right 2 m_
                                |> String.toInt
                                |> Maybe.andThen intToMonth
                            )
                            (String.right 2 d_ |> String.toInt)
                            |> Maybe.withDefault (D.fail ("Unknown dat value: " ++ value))

                    _ ->
                        D.fail ("Unknown date value: " ++ value)
            )


origin : Date
origin =
    Time.millisToPosix 0


date : Int -> Time.Month -> Int -> Maybe Date
date year month day =
    Calendar.fromRawParts { day = day, month = month, year = year }
        |> Maybe.map (flip DateTime.fromDateAndTime Clock.midnight)
        |> Maybe.map DateTime.toPosix


isYear : Int -> Date -> Bool
isYear year d =
    year == Time.toYear Time.utc d


isMonth : Time.Month -> Date -> Bool
isMonth month d =
    month == Time.toMonth Time.utc d


happendOn : Int -> Time.Month -> Date -> Bool
happendOn year month d =
    isYear year d && isMonth month d


sameMonth : Date -> Date -> Bool
sameMonth left right =
    toUtcYear left == toUtcYear right && toUtcMonth left == toUtcMonth right


toUtcYear : Date -> Int
toUtcYear =
    Time.toYear Time.utc


toUtcMonth : Date -> Time.Month
toUtcMonth =
    Time.toMonth Time.utc


toMY : Date -> MY
toMY =
    DerberosCalendar.getFirstDayOfMonth Time.utc >> Time.posixToMillis


toDMYString : Time.Zone -> Date -> String
toDMYString zone =
    format config "%-d %B %Y" zone


toMYString : Time.Zone -> Date -> String
toMYString zone =
    format config "%B %Y" zone


intToMonth : Int -> Maybe Time.Month
intToMonth i =
    case i of
        1 ->
            Just Time.Jan

        2 ->
            Just Time.Feb

        3 ->
            Just Time.Mar

        4 ->
            Just Time.Apr

        5 ->
            Just Time.May

        6 ->
            Just Time.Jun

        7 ->
            Just Time.Jul

        8 ->
            Just Time.Aug

        9 ->
            Just Time.Sep

        10 ->
            Just Time.Oct

        11 ->
            Just Time.Nov

        12 ->
            Just Time.Dec

        _ ->
            Nothing

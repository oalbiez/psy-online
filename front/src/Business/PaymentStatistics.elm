module Business.PaymentStatistics exposing (Statistics, add, averagePaymentPerHour, fromPayment, fromPayments, session, support, total, workshop, zero)

import Business.Duration as Duration
import Business.Payment as Payment
import Business.Price as Price


type alias Statistics =
    { support : Price.Price
    , workshops : Price.Price
    , sessions : Price.Price
    , duration : Duration.Duration
    }


zero : Statistics
zero =
    { support = Price.zero
    , workshops = Price.zero
    , sessions = Price.zero
    , duration = Duration.zero
    }


support : Price.Price -> Statistics
support amount =
    { support = amount
    , workshops = Price.zero
    , sessions = Price.zero
    , duration = Duration.zero
    }


workshop : Price.Price -> Statistics
workshop amount =
    { support = Price.zero
    , workshops = amount
    , sessions = Price.zero
    , duration = Duration.zero
    }


session : Duration.Duration -> Price.Price -> Statistics
session duration amount =
    { support = Price.zero
    , workshops = Price.zero
    , sessions = amount
    , duration = duration
    }


fromPayment : Payment.Payment -> Statistics
fromPayment payment =
    payment.amount
        |> (case payment.reason of
                Payment.Session duration ->
                    session duration

                Payment.Workshop ->
                    workshop

                Payment.Support ->
                    support
           )


fromPayments : List Payment.Payment -> Statistics
fromPayments =
    List.map fromPayment >> List.foldr add zero


add : Statistics -> Statistics -> Statistics
add left right =
    { support = Price.add left.support right.support
    , workshops = Price.add left.workshops right.workshops
    , sessions = Price.add left.sessions right.sessions
    , duration = Duration.add left.duration right.duration
    }


averagePaymentPerHour : Statistics -> Price.Price
averagePaymentPerHour s =
    let
        ratio =
            s.duration |> Duration.toHours |> Price.div
    in
    s.sessions |> ratio


total : Statistics -> Price.Price
total s =
    s.support
        |> Price.add s.workshops
        |> Price.add s.sessions

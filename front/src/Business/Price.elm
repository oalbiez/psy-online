module Business.Price exposing (Price(..), add, amount, div, fromFloat, fromInt, fromString, mul, sub, sum, toString, zero)

import FormatNumber exposing (format)
import FormatNumber.Locales exposing (Locale, frenchLocale)
import Maybe exposing (Maybe)


type Price
    = Price Float


zero : Price
zero =
    fromFloat 0.0


fromInt : Int -> Price
fromInt =
    toFloat >> (*) 0.01 >> fromFloat


fromFloat : Float -> Price
fromFloat value =
    value |> Price


fromString : String -> Maybe Price
fromString value =
    if String.endsWith "€" value then
        value |> String.dropRight 1 |> String.toFloat |> Maybe.map fromFloat

    else
        Nothing


amount : Price -> Float
amount (Price value) =
    value


add : Price -> Price -> Price
add (Price left) (Price right) =
    left + right |> fromFloat


sub : Price -> Price -> Price
sub (Price left) (Price right) =
    left - right |> fromFloat


mul : Float -> Price -> Price
mul factor (Price price) =
    price * factor |> fromFloat


div : Float -> Price -> Price
div factor (Price price) =
    price / factor |> fromFloat


sum : List Price -> Price
sum =
    List.foldr add zero


priceLocale : Locale
priceLocale =
    { frenchLocale
        | decimals = 2
    }


toString : Price -> String
toString (Price value) =
    format priceLocale value ++ "€"

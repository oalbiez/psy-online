module Business.WorkStatistics exposing (Statistics, add, fromWork, fromWorks, total, zero)

import Business.Duration as Duration
import Business.Work as Work


type alias Statistics =
    { individual : Duration.Duration
    , workshop : Duration.Duration
    , volonteer : Duration.Duration
    }


zero : Statistics
zero =
    { individual = Duration.zero
    , workshop = Duration.zero
    , volonteer = Duration.zero
    }


individual : Duration.Duration -> Statistics
individual duration =
    { zero | individual = duration }


workshop : Duration.Duration -> Statistics
workshop duration =
    { zero | workshop = duration }


volonteer : Duration.Duration -> Statistics
volonteer duration =
    { zero | volonteer = duration }


add : Statistics -> Statistics -> Statistics
add left right =
    { individual = Duration.add left.individual right.individual
    , workshop = Duration.add left.workshop right.workshop
    , volonteer = Duration.add left.volonteer right.volonteer
    }


total : Statistics -> Duration.Duration
total s =
    s.individual
        |> Duration.add s.workshop
        |> Duration.add s.volonteer


fromWork : Work.Work -> Statistics
fromWork work =
    work.duration
        |> (case work.category of
                Work.Individual ->
                    individual

                Work.Workshop ->
                    workshop

                Work.Volunteer ->
                    volonteer
           )


fromWorks : List Work.Work -> Statistics
fromWorks =
    List.map fromWork >> List.foldr add zero

module Business.Payment exposing (Payment, Reason(..), averagePaymentPerHour, balanceByMonth, byMonth)

import Business.Date as Date
import Business.Duration as Duration
import Business.Price as Price
import Dict
import Dict.Extra as DictExtra


type Reason
    = Session Duration.Duration
    | Workshop
    | Support


type alias Payment =
    { when : Date.Date
    , amount : Price.Price
    , reason : Reason
    }


isSessionPayment : Payment -> Bool
isSessionPayment payment =
    case payment.reason of
        Session _ ->
            True

        _ ->
            False


paymentPerHour : Payment -> Price.Price
paymentPerHour payment =
    case payment.reason of
        Session duration ->
            Price.div (Duration.toHours duration) payment.amount

        _ ->
            Price.zero


averagePaymentPerHour : List Payment -> Price.Price
averagePaymentPerHour payments =
    let
        sessionPayments =
            payments
                |> List.filter isSessionPayment

        ratio =
            sessionPayments |> List.length |> toFloat |> Price.div
    in
    sessionPayments
        |> List.map paymentPerHour
        |> List.foldr Price.add Price.zero
        |> ratio


balance : List Payment -> Price.Price
balance =
    List.map .amount >> Price.sum


byMonth : List Payment -> Dict.Dict Date.MY (List Payment)
byMonth =
    DictExtra.groupBy (\p -> p.when |> Date.toMY)


balanceByMonth : List Payment -> Dict.Dict Date.MY Price.Price
balanceByMonth =
    byMonth >> Dict.map (\_ -> balance)

module Business.Statistics exposing (Statistics, today, updateWorks)

import Business.Date as Date
import Business.Payment as Payment
import Business.Work as Work


type alias Statistics =
    { today : Date.Date
    , work : List Work.Work
    , payments : List Payment.Payment
    }


updateWorks : List Work.Work -> Statistics -> Statistics
updateWorks works statistics =
    { statistics | work = works }


today : Date.Date -> Statistics -> Statistics
today value statistics =
    { statistics | today = value }

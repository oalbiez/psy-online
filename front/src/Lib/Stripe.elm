port module Lib.Stripe exposing (ClientCheckout, PriceRef(..), clientCheckout)


type PriceRef
    = PriceRef String


type alias ClientCheckout =
    { price : PriceRef
    , quantity : Int
    }


clientCheckout : ClientCheckout -> Cmd msg
clientCheckout data =
    let
        ref (PriceRef v) =
            v
    in
    stripeClientCheckout ( data.price |> ref, data.quantity )



---- PORTS ----


port stripeClientCheckout : ( String, Int ) -> Cmd msg

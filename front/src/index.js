import './main.css';
import { Elm } from './Main.elm';
import * as serviceWorker from './serviceWorker';




const app = Elm.Main.init({
  node: document.getElementById('root'),
  flags: { 'width': window.innerWidth, 'height': window.innerHeight }
});



if (app.ports) {

  const stripe = Stripe('pk_test_51GtOlTLjunRWsLP8rjAWSEpMdHXFVear0VtPbW4Z6NDIlaZW5n5VdRzvRJ97SscStwROIGoUCRacqRCiO8aOsElU00lkcet3tx');

  if (app.ports.stripeClientCheckout) {
    app.ports.stripeClientCheckout.subscribe(message => {
      console.log('checkout: ' + message);

      const [priceRef, quantity] = message
      stripe.redirectToCheckout({
        lineItems: [{
          price: priceRef,
          quantity: quantity,
        }],
        mode: 'payment',
        successUrl: 'https://keilana.albiez.eu/#payment/success',
        cancelUrl: 'https://keilana.albiez.eu/#payment/cancel',
      }).then(function (result) {
        console.log('checkout: ' + result);

        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
    });
  }
}


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
